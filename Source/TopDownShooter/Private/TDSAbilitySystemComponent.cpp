// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSAbilitySystemComponent.h"
#include "TDSAttributeSet.h"

UTDSAbilitySystemComponent::UTDSAbilitySystemComponent(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	bHasWeaponAttributes = false;
}

void UTDSAbilitySystemComponent::CheckOrInitWeaponAttributes()
{
	if (!bHasWeaponAttributes)
	{
		bHasWeaponAttributes = true;
		AddSet<UTDSWeaponAttributeSet>();
	}
}

void UTDSAbilitySystemComponent::RefreshWithWeaponData(const FWeaponData& InData)
{
	CheckOrInitWeaponAttributes();
	SetNumericAttributeBase(UTDSWeaponAttributeSet::GetDamageAttribute(), InData.Damage);
	SetNumericAttributeBase(UTDSWeaponAttributeSet::GetMagazineSizeAttribute(), InData.MagazineSize);
	SetNumericAttributeBase(UTDSWeaponAttributeSet::GetReloadTimeAttribute(), InData.ReloadTime);
	SetNumericAttributeBase(UTDSWeaponAttributeSet::GetAccuracyAttribute(), InData.Accuracy);
	SetNumericAttributeBase(UTDSWeaponAttributeSet::GetRateOfFireAttribute(), InData.RateOfFire);
	SetNumericAttributeBase(UTDSWeaponAttributeSet::GetAmmoReserveCostAttribute(), InData.AmmoReserveCost);
	SetNumericAttributeBase(UTDSWeaponAttributeSet::GetPelletsPerShotAttribute(), InData.PelletsPerShot);
}

void UTDSAbilitySystemComponent::RefreshWithWeaponDynamicData(const FWeaponDynamicData& InData)
{
	CheckOrInitWeaponAttributes();
	SetNumericAttributeBase(UTDSWeaponAttributeSet::GetAmmoInMagazineAttribute(), InData.AmmoInMagazine);
}

void UTDSAbilitySystemComponent::RefreshWithArmorData(const FArmorData& InData)
{
	SetNumericAttributeBase(UTDSAttributeSet::GetMaxArmorAttribute(), InData.MaxArmor);
	SetNumericAttributeBase(UTDSAttributeSet::GetArmorRegenRateAttribute(), InData.RegenRate);
	SetNumericAttributeBase(UTDSAttributeSet::GetArmorRegenDelayAttribute(), InData.RegenDelay);

	SetNumericAttributeBase(UTDSAttributeSet::GetArmorAttribute(), 
		FMath::Min(GetNumericAttribute(UTDSAttributeSet::GetArmorAttribute()),InData.MaxArmor));

	FGameplayEventData Payload;
	HandleGameplayEvent(FGameplayTag::RequestGameplayTag("Armor.Event.EquippedArmorChanged"), &Payload);
}

void UTDSAbilitySystemComponent::UpdateWeaponDynamicData(FWeaponDynamicData& InData)
{
	InData.AmmoInMagazine = GetNumericAttributeBase(UTDSWeaponAttributeSet::GetAmmoInMagazineAttribute());
}

const bool UTDSAbilitySystemComponent::IsAbilityActiveByClass(TSubclassOf<UGameplayAbility> AbilityClass)
{
	if (!AbilityClass) return false;

	const auto Spec = FindAbilitySpecFromClass(AbilityClass);
	if (!Spec) return false;
	
	return Spec->IsActive();
}

const bool UTDSAbilitySystemComponent::IsAbilityActiveByHandle(FGameplayAbilitySpecHandle& AbilitySpecHandle)
{
	if (!AbilitySpecHandle.IsValid()) return false;

	const auto Spec = FindAbilitySpecFromHandle(AbilitySpecHandle);
	if (!Spec) return false;

	return Spec->IsActive();
}

void UTDSAbilitySystemComponent::LocalInputConfirm()
{
	GenericLocalConfirmCallbacks.Broadcast();
}
