// Fill out your copyright notice in the Description page of Project Settings.


#include "MMC/TDSMMCHealthBasedSlow.h"
#include "TDSAttributeSet.h"

UTDSMMCHealthBasedSlow::UTDSMMCHealthBasedSlow()
{
	HealthDef.AttributeToCapture = UTDSAttributeSet::GetHealthAttribute();
	HealthDef.AttributeSource = EGameplayEffectAttributeCaptureSource::Target;
	HealthDef.bSnapshot = false;
	RelevantAttributesToCapture.Add(HealthDef);

	MaxHealthDef.AttributeToCapture = UTDSAttributeSet::GetMaxHealthAttribute();
	MaxHealthDef.AttributeSource = EGameplayEffectAttributeCaptureSource::Target;
	MaxHealthDef.bSnapshot = false;
	RelevantAttributesToCapture.Add(MaxHealthDef);

	HasCalcedOnce = false;
	OldVal = 0;
}

float UTDSMMCHealthBasedSlow::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
	FAggregatorEvaluateParameters EvalParams;
	
	float Health = 0;
	GetCapturedAttributeMagnitude(HealthDef, Spec, EvalParams, Health);
	
	float MaxHealth = 0;
	GetCapturedAttributeMagnitude(MaxHealthDef, Spec, EvalParams, MaxHealth);

	return (MaxHealth == 0) ? 1.f : 1.f - Health / MaxHealth;
}
