// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSMMCLostHealthStack.h"
#include "TDSAttributeSet.h"

UTDSMMCLostHealthStack::UTDSMMCLostHealthStack()
{
	ArmorDef.AttributeToCapture = UTDSAttributeSet::GetArmorAttribute();
	ArmorDef.AttributeSource = EGameplayEffectAttributeCaptureSource::Target;
	ArmorDef.bSnapshot = false;
	RelevantAttributesToCapture.Add(ArmorDef);
	
	HealthDef.AttributeToCapture = UTDSAttributeSet::GetHealthAttribute();
	HealthDef.AttributeSource = EGameplayEffectAttributeCaptureSource::Target;
	HealthDef.bSnapshot = false;
	RelevantAttributesToCapture.Add(HealthDef);

	MaxHealthDef.AttributeToCapture = UTDSAttributeSet::GetMaxHealthAttribute();
	MaxHealthDef.AttributeSource = EGameplayEffectAttributeCaptureSource::Target;
	MaxHealthDef.bSnapshot = false;
	RelevantAttributesToCapture.Add(MaxHealthDef);
}

float UTDSMMCLostHealthStack::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
	FAggregatorEvaluateParameters EvalParams;
	
	float Armor = 0;
	GetCapturedAttributeMagnitude(ArmorDef, Spec, EvalParams, Armor);

	float Health = 0;
	GetCapturedAttributeMagnitude(HealthDef, Spec, EvalParams, Health);

	float MaxHealth = 0;
	GetCapturedAttributeMagnitude(MaxHealthDef, Spec, EvalParams, MaxHealth);

	return (Armor > 0 || MaxHealth == 0) ? 0 : FMath::FloorToFloat((1.f - (Health / MaxHealth)) * 10);
}
