// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSWeapon.h"
#include "AbilitySystemComponent.h"
#include "AttributeSet.h"
#include "TDSWeaponAttributeSet.h"

// Sets default values
ATDSWeapon::ATDSWeapon()
{
}

USoundBase* ATDSWeapon::GetRandomSound()
{
	if (SoundArray.Num() == 0) return nullptr;

	return SoundArray[FMath::RandHelper(SoundArray.Num())];
}
