// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSPlayerCharacter.h"

#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "AbilitySystemComponent.h"
#include "Components/InputComponent.h"
#include "TDSAttributeSet.h"
#include "TDSWeaponAttributeSet.h"
#include "WidgetComponent.h"
#include "TopDownShooter.h"
#include "TDSWeapon.h"
#include "TDSAbilitySystemComponent.h"
#include "TDSSurvivalPlayerState.h"
#include "TDSKiosk.h"
#include "TDSAbilityEquipment.h"
#include "Equipment/TDSWeaponEquipment.h"
#include "Equipment/TDSArmorEquipment.h"

// Sets default values
ATDSPlayerCharacter::ATDSPlayerCharacter(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bIncapacitated = false;

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Boom"));
	CameraBoom->SetupAttachment(RootComponent);

	CameraBoom->bInheritPitch = CameraBoom->bInheritRoll = CameraBoom->bInheritYaw = false;
	CameraBoom->bIsCameraFixed = false;
	CameraBoom->bEnableCameraLag = true;
	CameraBoom->TargetArmLength = 1200.0f;
	CameraBoom->bDoCollisionTest = false;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Follow Camera"));

	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	FloatingWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Floating Widget"));
	FloatingWidget->SetupAttachment(RootComponent);
	FloatingWidget->SetWidgetSpace(EWidgetSpace::Screen);
	FloatingWidget->SetRelativeLocation(FVector(0, 0, 100.0f));

	Faction = ETDSFaction::GoodGuys;
}

// Called when the game starts or when spawned
void ATDSPlayerCharacter::BeginPlay()
{
	if (HasAuthority())
	{
		ApplyDefaultAbilities();
	}
	
	Super::BeginPlay();

	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(
		UTDSAttributeSet::GetHealthAttribute()
	).AddUObject(this, &ATDSPlayerCharacter::OnHealthChanged);

	AbilitySystemComponent->ApplyGameplayEffectToSelf(
		DashRechargeEffect.GetDefaultObject(),
		0,
		AbilitySystemComponent->MakeEffectContext());

	//if(GetWorld()->GetAuthGameMode<ATDSGameModeBase>())
}

void ATDSPlayerCharacter::MoveForward(float Value)
{
	AddMovementInput(FVector(1.0f, 0, 0) * Value);
}

void ATDSPlayerCharacter::MoveRight(float Value)
{
	AddMovementInput(FVector(0, 1.0f, 0) * Value);
}

void ATDSPlayerCharacter::ApplyDefaultAbilities()
{

	if (ArmorRegenAbility)
	{
		AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(ArmorRegenAbility.GetDefaultObject(), 1,
			static_cast<int32>(EPlayerAbilityInput::None)));
	}

	if(ReloadAbility)
	{
		AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(ReloadAbility.GetDefaultObject(), 1,
			static_cast<int32>(EPlayerAbilityInput::Reload)));
	}

	if (MovementAbility)
	{
		MovementAbilityHandle = AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(MovementAbility.GetDefaultObject(), 1,
			static_cast<int32>(EPlayerAbilityInput::UseMovementAbility)));
	}

	if (ActiveAbility)
	{
		ActiveAbilityHandle = AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(ActiveAbility.GetDefaultObject(), 1,
			static_cast<int32>(EPlayerAbilityInput::UseActiveAbility)));
	}

	if (ReactiveAbility)
	{
		ReactiveAbilityHandle = AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(ReactiveAbility.GetDefaultObject(), 1,
			static_cast<int32>(EPlayerAbilityInput::None)));
	}

	if (PassiveAbility)
	{
		PassiveAbilityHandle = AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(PassiveAbility.GetDefaultObject(), 1,
			static_cast<int32>(EPlayerAbilityInput::None)));
	}

	if (OverdriveAbility)
	{
		OverdriveAbilityHandle = AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(OverdriveAbility.GetDefaultObject(), 1,
			static_cast<int32>(EPlayerAbilityInput::UseOverdriveAbility)));
	}
}

void ATDSPlayerCharacter::OnHealthChanged(const FOnAttributeChangeData& Data)
{
	if (bIncapacitated) return;

	if (Data.NewValue == 0) OnIncapacitated();
}

void ATDSPlayerCharacter::OnIncapacitated()
{
	bIncapacitated = true;

	DisableInput(Cast<APlayerController>(GetController()));
	bUseControllerRotationYaw = false;
	GetCharacterMovement()->bUseControllerDesiredRotation = false;
	AbilitySystemComponent->CancelAllAbilities();
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
	GetCharacterMovement()->Deactivate();

	OnIncapacitatedImplementable();

	OnPlayerCharacterIncapacitated.Broadcast(this);
}

void ATDSPlayerCharacter::OnRevived()
{
	if (!bIncapacitated) return;
	
	EnableInput(Cast<APlayerController>(GetController()));
	bIncapacitated = false;
	AbilitySystemComponent->SetNumericAttributeBase(UTDSAttributeSet::GetHealthAttribute(), 50.f);
	GetCharacterMovement()->Activate();
	GetCharacterMovement()->bUseControllerDesiredRotation = true;
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::Type::QueryAndPhysics);
	AbilitySystemComponent->TryActivateAbilityByClass(ArmorRegenAbility);

	OnRevivedImplementable();
	
	OnPlayerCharacterRevived.Broadcast(this);
}

bool ATDSPlayerCharacter::EquipWeapon(UTDSWeaponEquipment* WeaponEquipment, bool bForceEquip)
{
	if(EquippedWeapon && !bForceEquip)
	{
		return false;
	}
	
	AbilitySystemComponent->RefreshWithWeaponData(WeaponEquipment->GetWeaponData());
	AbilitySystemComponent->RefreshWithWeaponDynamicData(WeaponEquipment->GetWeaponDynamicData());

	if(WeaponEquipment->GetWeaponAbility())
	{
		WeaponMainAbilityHandle = AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(WeaponEquipment->GetWeaponAbility().GetDefaultObject(), 1,
			static_cast<int32>(EPlayerAbilityInput::UseWeaponMainAbility)));
	}
	
	EquippedWeapon = WeaponEquipment;

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	EquippedWeaponActor = EquippedWeapon->MakeWeaponActor(GetWorld(), GetActorTransform(), SpawnParams);
	EquippedWeaponActor->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, "WeaponSocket");
	OnWeaponActorChanged(EquippedWeaponActor);
	
	OnEquippedWeaponChange.Broadcast(EquippedWeapon);

	return true;
	
}

bool ATDSPlayerCharacter::EquipArmor(UTDSArmorEquipment* ArmorEquipment, bool bForceEquip)
{
	if(EquippedArmor != nullptr && !bForceEquip)
	{
		return false;
	}
	
	AbilitySystemComponent->RefreshWithArmorData(ArmorEquipment->GetArmorData());

	EquippedArmor = ArmorEquipment;
	
	OnEquippedArmorChange.Broadcast(EquippedArmor);
	return true;
}

bool ATDSPlayerCharacter::EquipAbility(UTDSAbilityEquipment* AbilityEquipment)
{
	auto EquipStatus = CheckEquipAbilityStatus(AbilityEquipment);
	if (EquipStatus == EEquipAbilityStatus::EAS_Reject) return false;
	
	UTDSAbilityEquipment** TargetAbilityEquipmentPtr = nullptr;
	TSubclassOf<class UGameplayAbility>* TargetAbilityPtr = nullptr;
	FGameplayAbilitySpecHandle* TargetAbilityHandlePtr = nullptr;
	EPlayerAbilityInput TargetInput = EPlayerAbilityInput::None;
	
	switch(AbilityEquipment->GetAbilityData().AbilityType)
	{
		case EAbilityType::AT_Movement:
			TargetAbilityEquipmentPtr = &EquippedMovementAbility;
			TargetAbilityPtr = &MovementAbility;
			TargetAbilityHandlePtr = &MovementAbilityHandle;
			TargetInput = EPlayerAbilityInput::UseMovementAbility;
			break;
		
		case EAbilityType::AT_Active:
			TargetAbilityEquipmentPtr = &EquippedActiveAbility;
			TargetAbilityPtr = &ActiveAbility;
			TargetAbilityHandlePtr = &ActiveAbilityHandle;
			TargetInput = EPlayerAbilityInput::UseActiveAbility;
			break;
		
		case EAbilityType::AT_Reactive:
			TargetAbilityEquipmentPtr = &EquippedReactiveAbility;
			TargetAbilityPtr = &ReactiveAbility;
			TargetAbilityHandlePtr = &ReactiveAbilityHandle;
			break;
		
		case EAbilityType::AT_Passive:
			TargetAbilityEquipmentPtr = &EquippedPassiveAbility;
			TargetAbilityPtr = &PassiveAbility;
			TargetAbilityHandlePtr = &PassiveAbilityHandle;
			break;
		
		case EAbilityType::AT_Overdrive:
			TargetAbilityEquipmentPtr = &EquippedOverdriveAbility;
			TargetAbilityPtr = &OverdriveAbility;
			TargetAbilityHandlePtr = &OverdriveAbilityHandle;
			TargetInput = EPlayerAbilityInput::UseOverdriveAbility;
			break;
		default: ;
	}

	if(TargetAbilityHandlePtr)
	{
		if(AbilitySystemComponent->IsAbilityActiveByHandle(*TargetAbilityHandlePtr))
		{
			AbilitySystemComponent->CancelAbilityHandle(*TargetAbilityHandlePtr);
		}

		if(*TargetAbilityPtr)
		{
			AbilitySystemComponent->RemoveActiveEffectsWithGrantedTags(*TargetAbilityPtr->GetDefaultObject()->GetCooldownTags());
			AbilitySystemComponent->ClearAbility(*TargetAbilityHandlePtr);
		}
	}

	if(EquipStatus == EEquipAbilityStatus::EAS_Upgrade)
	{
		// Skip null-checking TargetAbilityEquipmentPtr because it's already done in CheckEquipStatus
		(*TargetAbilityEquipmentPtr)->Upgrade();
		*TargetAbilityPtr = (*TargetAbilityEquipmentPtr)->GetAbility();
	}else
	{
		// Equip or Override
		*TargetAbilityEquipmentPtr = AbilityEquipment;
		*TargetAbilityPtr = AbilityEquipment->GetAbility();
	}
	
	*TargetAbilityHandlePtr = AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(TargetAbilityPtr->GetDefaultObject(),
		(*TargetAbilityEquipmentPtr)->GetAbilityLevel(),
		static_cast<int32>(TargetInput)));

	OnEquippedAbilityChange.Broadcast(*TargetAbilityEquipmentPtr);

	return true;
}

bool ATDSPlayerCharacter::UnequipWeapon(UTDSWeaponEquipment*& OutEquipment, bool bDeferBroadcast)
{
	if (!EquippedWeapon) return false;

	if (WeaponMainAbilityHandle.IsValid())
	{
		AbilitySystemComponent->ClearAbility(WeaponMainAbilityHandle);
		AbilitySystemComponent->CancelAbilityHandle(WeaponMainAbilityHandle);
	}

	if (WeaponOffhandAbilityHandle.IsValid())
	{
		AbilitySystemComponent->ClearAbility(WeaponOffhandAbilityHandle);
		AbilitySystemComponent->CancelAbilityHandle(WeaponOffhandAbilityHandle);
	}

	AbilitySystemComponent->UpdateWeaponDynamicData(EquippedWeapon->GetWeaponDynamicDataRef());

	if (EquippedWeaponActor) {
		EquippedWeaponActor->Destroy();
		OnWeaponActorChanged(nullptr);
	}

	OutEquipment = EquippedWeapon;
	EquippedWeapon = nullptr;

	if (!bDeferBroadcast) OnEquippedWeaponChange.Broadcast(EquippedWeapon);

	return true;
}

bool ATDSPlayerCharacter::UnequipArmor(UTDSArmorEquipment*& OutEquipment, bool bDeferBroadcast)
{
	if (!EquippedArmor) return false;
	
	AbilitySystemComponent->RefreshWithArmorData(FArmorData());

	OutEquipment = EquippedArmor;
	EquippedArmor = nullptr;

	if (!bDeferBroadcast) OnEquippedArmorChange.Broadcast(EquippedArmor);

	return true;
}

bool ATDSPlayerCharacter::UnequipAndHandleEquipment(UTDSEquipment* Equipment, EExistingEquipmentPolicy ExistingEquipmentPolicy)
{
	if (!Equipment) return false;
	
	auto GameState = Cast<ATDSSurvivalGameState>(
		GetWorld()->GetGameState()
		);

	switch(Equipment->GetType())
	{
		case EEquipmentType::EET_Weapon:
			UTDSWeaponEquipment* UnequippedWeapon;
			if (Equipment != EquippedWeapon) return false;

			if (ExistingEquipmentPolicy == EExistingEquipmentPolicy::EEEP_Sell)
			{
				if(!UnequipWeapon(UnequippedWeapon)) return false;
				
				GameState->AddCredits(UnequippedWeapon->GetPrice() / 2);
				return true;
			}
			else
			{
				auto EmptySlot = GameState->FindEmptyStorageSlot();
				if (EmptySlot < 0 || !UnequipWeapon(UnequippedWeapon)) return false;
				
				GameState->PutEquipmentToStorageSlot(UnequippedWeapon, EmptySlot);
				return true;
			}
		
		case EEquipmentType::EET_Armor:

			UTDSArmorEquipment* UnequippedArmor;
			if (Equipment != EquippedArmor) return false;

			if (ExistingEquipmentPolicy == EExistingEquipmentPolicy::EEEP_Sell)
			{
				if (!UnequipArmor(UnequippedArmor)) return false;

				GameState->AddCredits(UnequippedArmor->GetPrice() / 2);
				return true;
			}
			else
			{
				auto EmptySlot = GameState->FindEmptyStorageSlot();
				if (EmptySlot < 0 || !UnequipArmor(UnequippedArmor)) return false;

				GameState->PutEquipmentToStorageSlot(UnequippedArmor, EmptySlot);
				return true;
			}
	}

	return false;
}

// Called every frame
void ATDSPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATDSPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);


	PlayerInputComponent->BindAxis("MoveForward", this, &ATDSPlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATDSPlayerCharacter::MoveRight);

	AbilitySystemComponent->BindAbilityActivationToInputComponent(PlayerInputComponent,
		FGameplayAbilityInputBinds("ConfirmInput", "CancelInput", "EPlayerAbilityInput"));
}

FPlayerCharacterInfo ATDSPlayerCharacter::GetCharacterInfo() const
{
	return PlayerCharacterInfo;
}

void ATDSPlayerCharacter::SetCharacterInfo(const FPlayerCharacterInfo& InInfo)
{
	PlayerCharacterInfo = InInfo;
}

EEquipAbilityStatus ATDSPlayerCharacter::CheckEquipAbilityStatus(const UTDSAbilityEquipment* InEquipment)
{
	UTDSAbilityEquipment** AbilitySlot = nullptr;
	switch(InEquipment->GetAbilityData().AbilityType)
	{
		case EAbilityType::AT_Active: AbilitySlot = &EquippedActiveAbility; break;
		case EAbilityType::AT_Passive: AbilitySlot = &EquippedPassiveAbility; break;
		case EAbilityType::AT_Overdrive: AbilitySlot = &EquippedActiveAbility; break;
		default: return EEquipAbilityStatus::EAS_Reject;
	}

	if(*AbilitySlot == nullptr)
	{
		return EEquipAbilityStatus::EAS_Equip;
	}
	else if (!(*AbilitySlot)->IsSame(InEquipment)) {
		return EEquipAbilityStatus::EAS_Override;
	}
	else if ((*AbilitySlot)->CanUpgrade())
	{
		return EEquipAbilityStatus::EAS_Upgrade;
	}

	return EEquipAbilityStatus::EAS_Reject;
}

int ATDSPlayerCharacter::GetEquippedAbilityLevel(EAbilityType AbilityType) const
{
	switch (AbilityType)
	{
	case EAbilityType::AT_Active: return EquippedActiveAbility ? EquippedActiveAbility->GetAbilityLevel() : -1;
	case EAbilityType::AT_Passive: return EquippedPassiveAbility ? EquippedPassiveAbility->GetAbilityLevel() : -1;
	case EAbilityType::AT_Overdrive: return EquippedOverdriveAbility ? EquippedOverdriveAbility->GetAbilityLevel() : -1;
	default: return -1;
	}
}

bool ATDSPlayerCharacter::IsIncapacitated() const
{
	return bIncapacitated;
}

void ATDSPlayerCharacter::Revive()
{
	OnRevived();
}

void ATDSPlayerCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	if(auto PlayerController = Cast<APlayerController>(NewController))
	{
		if (PlayerController->GetLocalPlayer()) {
			PlayerId = PlayerController->GetLocalPlayer()->GetControllerId();
		}
	}
	if (AbilitySystemComponent) AbilitySystemComponent->RefreshAbilityActorInfo();
}

const FWeaponData ATDSPlayerCharacter::GetEquippedWeaponData()
{
	if(!EquippedWeapon)
	{
		FWeaponData EmptyData;
		EmptyData.Name = FText::FromString("No Weapon");
		return EmptyData;
	}
	return EquippedWeapon->GetWeaponData();
}

EWeaponStance ATDSPlayerCharacter::GetWeaponStance() const
{
	return EquippedWeaponActor ? EquippedWeaponActor->WeaponStance : EWeaponStance::WS_Empty;
}

ATDSWeapon* ATDSPlayerCharacter::GetWeaponActor()
{
	return EquippedWeaponActor;
}

const FArmorData ATDSPlayerCharacter::GetEquippedArmorData()
{
	if (!EquippedWeapon)
	{
		FArmorData EmptyData;
		EmptyData.Name = FText::FromString("No Armor");
		return EmptyData;
	}
	return EquippedArmor->GetArmorData();
}

