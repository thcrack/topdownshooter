// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSCameraAnchor.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "WidgetLayoutLibrary.h"

// Sets default values
ATDSCameraAnchor::ATDSCameraAnchor()
{
 	// Let TDSGameModeBase tick for us
	PrimaryActorTick.bCanEverTick = false;

	CameraMinHeight = 1500;
	CameraMaxHeight = 2500;
	ActorInclusionMargin = 500;
	TargetHeight = CameraMinHeight;
	
	CameraArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Arm"));
	CameraArm->TargetArmLength = TargetHeight;
	CameraArm->bDoCollisionTest = false;
	CameraArm->bEnableCameraLag = true;
	CameraArm->CameraLagSpeed = 5;
	SetRootComponent(CameraArm);

	ViewTarget = CreateDefaultSubobject<UCameraComponent>(TEXT("View Target"));
	ViewTarget->SetupAttachment(CameraArm, USpringArmComponent::SocketName);
	CameraArm->SetRelativeRotation(FRotator::MakeFromEuler(FVector(0, -85, 0)));

}

// Called when the game starts or when spawned
void ATDSCameraAnchor::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATDSCameraAnchor::UpdateAnchor(const TArray<AActor*>& ActorsToFit, float DeltaSeconds)
{
	if (ActorsToFit.Num() == 0) return;
	
	auto ViewportSize = UWidgetLayoutLibrary::GetViewportSize(this);
	auto ViewportHeightToWidthRatio = ViewportSize.Y / ViewportSize.X;

	FVector MidPoint(0, 0, 0);
	for (auto Actor : ActorsToFit)
	{
		MidPoint += Actor->GetActorLocation();
	}
	MidPoint /= ActorsToFit.Num();

	SetActorLocation(MidPoint);

	float LargestDistanceToMidPoint = 0;
	for (auto Actor : ActorsToFit)
	{
		auto Dist = FVector::DistSquared(MidPoint, Actor->GetActorLocation());
		if (Dist > LargestDistanceToMidPoint) LargestDistanceToMidPoint = Dist;
	}
	if (LargestDistanceToMidPoint > 0) LargestDistanceToMidPoint = FMath::Sqrt(LargestDistanceToMidPoint);
	LargestDistanceToMidPoint += ActorInclusionMargin;

	auto HalfSmallestFOVAngle = ViewTarget->FieldOfView * ViewportHeightToWidthRatio / 2;
	auto IdealHeight = LargestDistanceToMidPoint * FMath::Tan(PI / (180.f) * (90.f - HalfSmallestFOVAngle));
	TargetHeight = FMath::Clamp(IdealHeight, CameraMinHeight, CameraMaxHeight);
	
	CameraArm->TargetArmLength = FMath::Lerp(CameraArm->TargetArmLength, TargetHeight, CameraArm->CameraLagSpeed * DeltaSeconds);

}
