// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/TDSCharacterBase.h"
#include "Components/TDSCharacterMovementComponent.h"
#include "TDSAbilitySystemComponent.h"
#include "TDSAttributeSet.h"
#include "Engine/Engine.h"
#include "TDSCombatText.h"
#include "Kismet/KismetTextLibrary.h"

// Sets default values
ATDSCharacterBase::ATDSCharacterBase(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UTDSCharacterMovementComponent>(CharacterMovementComponentName))
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bCreateCombatText = true;

	AbilitySystemComponent = CreateDefaultSubobject<UTDSAbilitySystemComponent>(TEXT("AbilitySystemComponent"));

	Faction = ETDSFaction::None;
	
}

void ATDSCharacterBase::OnReceiveDamage(float Damage, const FGameplayEffectContextHandle& ContextHandle)
{
	K2_OnReceiveDamage(Damage, ContextHandle);
	
	if (!CombatTextClass) return;

	//UE_LOG(LogTemp, Warning, TEXT("Damage: %f"), Damage);
	
	auto HitResult = ContextHandle.GetHitResult();
	auto Location = HitResult ? HitResult->ImpactPoint : (GetActorLocation() + FVector(0, 0, 150));
	ATDSCombatText* NewCombatText = Cast<ATDSCombatText>(GetWorld()->SpawnActor(CombatTextClass, &Location, &FRotator::ZeroRotator));
	NewCombatText->SetText(UKismetTextLibrary::Conv_FloatToText(Damage, 
		FromZero, false, false,
		1, 324,
		0, 0)
	);
	NewCombatText->Launch();
}

// Called when the game starts or when spawned
void ATDSCharacterBase::BeginPlay()
{

	AbilitySystemComponent->InitAbilityActorInfo(this, this);
	if (HasAuthority())
	{
		auto NewSet = AbilitySystemComponent->InitStats(UTDSAttributeSet::StaticClass(), AttributeInitDataTable);
		auto TDSAttributeSet = const_cast<UTDSAttributeSet*>(Cast<UTDSAttributeSet>(NewSet));
		TDSAttributeSet->OnAttributeSetReceiveDamage.AddDynamic(this, &ATDSCharacterBase::OnReceiveDamage);
	}

	Super::BeginPlay();
}

// Called every frame
void ATDSCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATDSCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UAbilitySystemComponent* ATDSCharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void ATDSCharacterBase::CausedDamageCallback_Implementation(float Damage, const FGameplayEffectContextHandle& ContextHandle)
{
	OnCausedDamageCallback.Broadcast(Damage, ContextHandle);
}

ETDSFaction ATDSCharacterBase::GetFaction_Implementation()
{
	return Faction;
}

