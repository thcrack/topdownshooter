// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSEnemySpawnPod.h"
#include "TDSSurvivalGameMode.h"
#include "Engine/World.h"

// Sets default values
ATDSEnemySpawnPod::ATDSEnemySpawnPod()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDSEnemySpawnPod::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDSEnemySpawnPod::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

TArray<ATDSEnemyCharacter*> ATDSEnemySpawnPod::SpawnSquad(const TArray<TSubclassOf<class ATDSEnemyCharacter>>& SquadInfo)
{
	TArray<ATDSEnemyCharacter*> ResultArray;

	for (int i = 0; i < SquadInfo.Num(); ++i)
	{
		auto SpawnPoint = GetSpawnPointSceneComponent(i);
		auto TargetTransform = SpawnPoint ? SpawnPoint->GetComponentTransform() : GetActorTransform();
		auto NewEnemy = Cast< ATDSEnemyCharacter>(GetWorld()->SpawnActor(SquadInfo[i], &TargetTransform));
		ResultArray.Add(NewEnemy);
	}

	return ResultArray;
}

