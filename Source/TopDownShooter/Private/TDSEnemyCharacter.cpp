// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSEnemyCharacter.h"
#include "TDSAbilitySystemComponent.h"
#include "Engine/Engine.h"

// Sets default values
ATDSEnemyCharacter::ATDSEnemyCharacter(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Faction = ETDSFaction::BadGuys;
	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
}

// Called when the game starts or when spawned
void ATDSEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATDSEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATDSEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ATDSEnemyCharacter::Attack_Implementation()
{
}
