// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSChargedAbility.h"
#include "AbilitySystemComponent.h"
#include "AbilityTask_WaitInputPress.h"

UTDSChargedAbility::UTDSChargedAbility()
{
	ActivateAbilityOnGranted = true;
}

void UTDSChargedAbility::StartRecharge(const FGameplayAbilityActorInfo* ActorInfo)
{
	if (RechargeCooldownHandle.IsValid()) return;

	UGameplayEffect* CooldownGE = GetCooldownGameplayEffect();
	if (CooldownGE)
	{
		FGameplayEffectSpecHandle SpecHandle = MakeOutgoingGameplayEffectSpec(CooldownGE->GetClass(), GetAbilityLevel());
		SpecHandle.Data.Get()->DynamicGrantedTags.AppendTags(CooldownTags);
		SpecHandle.Data.Get()->SetSetByCallerMagnitude(CooldownSetByCallerTag, CooldownDuration.GetValueAtLevel(GetAbilityLevel()));
		RechargeCooldownHandle = GetActorInfo().AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(
			FGameplayEffectSpec(
				CooldownGE,
				GetActorInfo().AbilitySystemComponent->MakeEffectContext()
		));

		auto RemovedDelegate = GetAbilitySystemComponentFromActorInfo()->OnGameplayEffectRemoved_InfoDelegate(RechargeCooldownHandle);
		RemovedDelegate->AddUObject(this, &UTDSChargedAbility::OnRecharge);
	}
}

void UTDSChargedAbility::OnRecharge(const FGameplayEffectRemovalInfo& RemovedInfo)
{
	auto ActorInfo = GetCurrentActorInfo();
	auto ASC = ActorInfo->AbilitySystemComponent;
	ChargeEffectHandle = ASC->ApplyGameplayEffectSpecToSelf(
		FGameplayEffectSpec(ChargeEffect.GetDefaultObject(),
			ASC->MakeEffectContext()
		));

	if(GetStackCount(GetCurrentActorInfo()) < MaxChargeCount)
	{
		StartRecharge(ActorInfo);
	}
}

void UTDSChargedAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	FGameplayEffectSpec EffectSpec(ChargeEffect.GetDefaultObject(),
		ActorInfo->AbilitySystemComponent->MakeEffectContext());

	int ChargeCount = FMath::Min(MaxChargeCount, ChargeCountOnStart);

	for (int i = 0; i < ChargeCount; ++i)
	{
		ChargeEffectHandle = ActorInfo->AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(EffectSpec);
	}

	if (ChargeCount < MaxChargeCount)
	{
		StartRecharge(ActorInfo);
	}

	auto PressTask = UAbilityTask_WaitInputPress::WaitInputPress(this);
	//PressTask->OnPress

	if (bHasBlueprintActivate) K2_ActivateAbility();
}

void UTDSChargedAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	auto ASC = GetAbilitySystemComponentFromActorInfo();
	if (RechargeCooldownHandle.IsValid()) ASC->RemoveActiveGameplayEffect(RechargeCooldownHandle);
	if (ChargeEffectHandle.IsValid()) ASC->RemoveActiveGameplayEffect(ChargeEffectHandle);

	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UTDSChargedAbility::ApplyCooldown(const FGameplayAbilitySpecHandle Handle,
                                       const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) const
{
	// Do nothing as we handle the recharge effect manually
}

int UTDSChargedAbility::GetStackCount(const FGameplayAbilityActorInfo* ActorInfo) const
{
	if (!ChargeEffectHandle.IsValid()) return 0;

	auto Effect = ActorInfo->AbilitySystemComponent->GetActiveGameplayEffect(ChargeEffectHandle);

	return Effect ? Effect->Spec.StackCount : 0;
}

void UTDSChargedAbility::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	Super::OnAvatarSet(ActorInfo, Spec);
}

void UTDSChargedAbility::CommitExecute(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo)
{
	Super::CommitExecute(Handle, ActorInfo, ActivationInfo);
	GetAbilitySystemComponentFromActorInfo()->RemoveActiveGameplayEffect(ChargeEffectHandle, 1);

	if (!RechargeCooldownHandle.IsValid()) StartRecharge(ActorInfo);
}
