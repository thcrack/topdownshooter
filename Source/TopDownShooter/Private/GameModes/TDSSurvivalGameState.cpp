// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSSurvivalGameState.h"
#include "TDSSurvivalPlayerState.h"
#include "TDSEquipment.h"
#include "TDSPlayerCharacter.h"

ATDSSurvivalGameState::ATDSSurvivalGameState()
{
	GamePhase = ESurvivalGamePhase::Pregame;

	EquipmentStorage.Init(nullptr, StorageSize);
}

void ATDSSurvivalGameState::SetCurrentWave(int InValue)
{
	CurrentWave = InValue;
	OnSurvivalGameStateUpdated.Broadcast(this);
}

void ATDSSurvivalGameState::SetEnemyCountLeftInWave(int InValue)
{
	EnemyCountLeftInWave = InValue;
	OnSurvivalGameStateUpdated.Broadcast(this);
}

void ATDSSurvivalGameState::SetEnemyCountSpawnedInWave(int InValue)
{
	EnemyCountSpawnedInWave = InValue;
	OnSurvivalGameStateUpdated.Broadcast(this);
}

void ATDSSurvivalGameState::SetWaveBreakTimeRemaining(float InValue)
{
	WaveBreakTimeRemaining = InValue;
	OnSurvivalGameStateUpdated.Broadcast(this);
}

void ATDSSurvivalGameState::SetGamePhase(ESurvivalGamePhase NewPhase)
{
	if(NewPhase != GamePhase)
	{
		GamePhase = NewPhase;
		OnSurvivalGamePhaseUpdated.Broadcast(this, NewPhase);
		OnSurvivalGameStateUpdated.Broadcast(this);
	}
}

void ATDSSurvivalGameState::AddCredits(float Amount)
{
	Credits += Amount;
	for (auto PlayerStateBase : PlayerArray)
	{
		auto PlayerState = Cast<ATDSSurvivalPlayerState>(PlayerStateBase);
		ensure(PlayerState);
		PlayerState->AddCredits(Amount);
	}
	OnCreditsChanged.Broadcast(Credits);
}

bool ATDSSurvivalGameState::HasEmptyStorageSlot() const
{
	return FindEmptyStorageSlot() != NOT_FOUND;
}

UTDSEquipment* ATDSSurvivalGameState::GetStorageEquipmentAt(int Index) const
{
	if(Index >= EquipmentStorage.Num())
	{
		UE_LOG(LogTemp, Warning, TEXT("Storage index out of bound!"));
		return nullptr;
	}

	return EquipmentStorage[Index];
}

UTDSEquipment* ATDSSurvivalGameState::RetrieveStorageEquipmentAt(int Index, bool bDeferBroadcast)
{
	if (Index >= EquipmentStorage.Num())
	{
		UE_LOG(LogTemp, Warning, TEXT("Storage index out of bound!"));
		return nullptr;
	}

	auto Temp = EquipmentStorage[Index];
	EquipmentStorage[Index] = nullptr;

	if(Temp && !bDeferBroadcast) OnStorageChanged.Broadcast(Index, EquipmentStorage[Index]);
	
	return Temp;
}

bool ATDSSurvivalGameState::GiveStorageEquipmentToPlayer(int Index, ATDSPlayerCharacter* PlayerCharacter)
{
	auto Equipment = RetrieveStorageEquipmentAt(Index);
	bool EquipSuccessful = false;
	if(Equipment)
	{
		switch (Equipment->GetType()) {
			case EEquipmentType::EET_Weapon:
				UTDSWeaponEquipment* ExistingWeapon;
				if(PlayerCharacter->UnequipWeapon(ExistingWeapon, true))
				{
					PutEquipmentToStorageSlot(ExistingWeapon, Index);
				}
				EquipSuccessful = PlayerCharacter->EquipWeapon(Cast<UTDSWeaponEquipment>(Equipment));
			break;
			
			case EEquipmentType::EET_Armor:
				UTDSArmorEquipment* ExistingArmor;
				if (PlayerCharacter->UnequipArmor(ExistingArmor, true))
				{
					PutEquipmentToStorageSlot(ExistingArmor, Index);
				}
				EquipSuccessful = PlayerCharacter->EquipArmor(Cast<UTDSArmorEquipment>(Equipment));
			break;
			
			case EEquipmentType::EET_Ability:
				EquipSuccessful = PlayerCharacter->EquipAbility(Cast<UTDSAbilityEquipment>(Equipment));
			break;

			default: return false;
		}
	}

	if(!EquipSuccessful)
	{
		RetrieveStorageEquipmentAt(Index);
		PutEquipmentToStorageSlot(Equipment, Index);
	}

	return EquipSuccessful;
}

bool ATDSSurvivalGameState::SellStorageEquipment(int Index)
{
	auto Equipment = RetrieveStorageEquipmentAt(Index);
	if (!Equipment) return false;

	AddCredits(Equipment->GetPrice() / 2);
	return true;
}

int ATDSSurvivalGameState::FindEmptyStorageSlot() const
{
	for (int i = 0; i < EquipmentStorage.Num(); ++i) if (!EquipmentStorage[i]) return i;
	
	return NOT_FOUND;
}

bool ATDSSurvivalGameState::PutEquipmentToStorageSlot(UTDSEquipment* Equipment, int SlotIndex)
{
	if (SlotIndex >= EquipmentStorage.Num())
	{
		UE_LOG(LogTemp, Warning, TEXT("Storage index out of bound!"));
		return false;
	}

	if (EquipmentStorage[SlotIndex])
	{
		UE_LOG(LogTemp, Warning, TEXT("Storage already has an equipment at the slot index!"));
		return false;
	}

	EquipmentStorage[SlotIndex] = Equipment;

	OnStorageChanged.Broadcast(SlotIndex, EquipmentStorage[SlotIndex]);
	
	return true;
}

bool ATDSSurvivalGameState::PutEquipmentToEmptyStorageSlot(UTDSEquipment* Equipment)
{
	auto FreeSlotIndex = FindEmptyStorageSlot();
	
	if(FreeSlotIndex == NOT_FOUND)
	{
		UE_LOG(LogTemp, Warning, TEXT("Storage is full!"));
		return false;
	}

	EquipmentStorage[FreeSlotIndex] = Equipment;

	OnStorageChanged.Broadcast(FreeSlotIndex, EquipmentStorage[FreeSlotIndex]);
	return true;
}
