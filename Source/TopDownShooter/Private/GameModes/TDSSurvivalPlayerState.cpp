// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSSurvivalPlayerState.h"

ATDSSurvivalPlayerState::ATDSSurvivalPlayerState()
{
	Credits = 0;
}

void ATDSSurvivalPlayerState::AddCredits(float Amount)
{
	Credits += Amount;
	OnCreditsChanged.Broadcast(Credits);
}

void ATDSSurvivalPlayerState::DeductCredits(float Amount)
{
	Credits -= Amount;
	OnCreditsChanged.Broadcast(Credits);
}
