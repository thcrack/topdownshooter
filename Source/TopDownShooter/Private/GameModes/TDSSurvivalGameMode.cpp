// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSSurvivalGameMode.h"
#include "TDSSurvivalGameState.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "TDSEnemyCharacter.h"
#include "TDSSurvivalPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/TargetPoint.h"
#include "TDSEnemySpawnPod.h"

ATDSSurvivalGameMode::ATDSSurvivalGameMode()
{
	GameStateClass = ATDSSurvivalGameState::StaticClass();
	PlayerStateClass = ATDSSurvivalPlayerState::StaticClass();
	PodSpawnPointClass = ATargetPoint::StaticClass();
}

float ATDSSurvivalGameMode::GetWaveBreakDurationLeft() const
{
	return WaveBreakTimerHandle.IsValid() ? GetWorldTimerManager().GetTimerRemaining(WaveBreakTimerHandle) : 0;
}

ATDSSurvivalGameState* ATDSSurvivalGameMode::GetSurvivalGameState()
{
	if (!SurvivalGameState)
	{
		SurvivalGameState = GetGameState<ATDSSurvivalGameState>();
	}
	
	return SurvivalGameState;
}

void ATDSSurvivalGameMode::FillCurrentWaveSquads(int EnemyCount)
{
	CurrentWaveSquads.Empty();
	auto EnemyCountToFill = EnemyCount;
	while (EnemyCountToFill > 0)
	{
		const auto SquadSize = FMath::Min(EnemyCountToFill, FMath::RandRange(3, 6));
		FSurvivalWaveSquadInfo NewSquad;
		for (auto i = 0; i < SquadSize; ++i)
		{
			NewSquad.Data.Add(PossibleEnemyClasses[FMath::RandHelper(PossibleEnemyClasses.Num())]);
		}
		EnemyCountToFill -= SquadSize;
		CurrentWaveSquads.Add(NewSquad);
	}
}

void ATDSSurvivalGameMode::StartPlay()
{
	Super::StartPlay();
	GetWorldTimerManager().SetTimer(WaveBreakTimerHandle, this, &ATDSSurvivalGameMode::StartWave, 3);
	GetWorldTimerManager().SetTimer(WaveBreakNotifyTimerHandle, this,
	 	&ATDSSurvivalGameMode::OnWaveBreakTimerUpdate, .25f, true);
}

void ATDSSurvivalGameMode::BeginPlay()
{
	// Load pod spawn points from level and init the array
	TArray<AActor*> OutActor;
	UGameplayStatics::GetAllActorsOfClass(this, PodSpawnPointClass, OutActor);
	PodSpawnPoints.Empty();
	for (auto Actor : OutActor)
	{
		PodSpawnPoints.Add(Cast<ATargetPoint>(Actor));
	}

	UE_LOG(LogTemp, Warning, TEXT("SurvivalMode: Found %d pod point(s) in the level"), OutActor.Num())
	
	if (WaveSetupTable)
	{
		TArray<FSurvivalWaveSetup*> Rows;
		WaveSetupTable->GetAllRows<FSurvivalWaveSetup>(TEXT("SurvivalMode"), Rows);
		
		WaveSetups.Empty();

		for (auto Row : Rows)
		{
			WaveSetups.Add(*Row);
		}
		
		UE_LOG(LogTemp, Warning, TEXT("SurvivalMode: Wave setup table found; loaded %d setups"), WaveSetups.Num());
	}
	
	Super::BeginPlay();
}

void ATDSSurvivalGameMode::StartWave()
{
	if(WaveBreakNotifyTimerHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(WaveBreakNotifyTimerHandle);
		OnWaveBreakTimerUpdate();
	}
	
	CurrentWave++;

	auto CurrentWavePodCount = 5;

	if(WaveSetups.Num() <= CurrentWave - 1)
	{
		// Wave exceeds loaded setups; make one with base and coefficient
		EnemyCountInCurrentWave = EnemyCountPerWaveBase + EnemyCountPerWaveCoefficient * CurrentWave;

		FillCurrentWaveSquads(EnemyCountInCurrentWave);
	}
	else if (!WaveSetups[CurrentWave - 1].bUsePredefinedSquadData)
	{
		CurrentWavePodCount = WaveSetups[CurrentWave - 1].PodCount;
		
		// Wave setup doesn't use predefined data; make one with provided enemy count
		EnemyCountInCurrentWave = WaveSetups[CurrentWave - 1].TotalEnemyInWave;

		FillCurrentWaveSquads(EnemyCountInCurrentWave);
	}
	else
	{
		CurrentWavePodCount = WaveSetups[CurrentWave - 1].PodCount;
		
		// Wave setup has predefined data
		CurrentWaveSquads = WaveSetups[CurrentWave - 1].PredefinedSquads;
		EnemyCountInCurrentWave = 0;
		for (auto CurrentWaveSquad : CurrentWaveSquads)
		{
			EnemyCountInCurrentWave += CurrentWaveSquad.Data.Num();
		}
	}

	/** Pod Spawning **/
	/* TODO: Nearest points to player */
	
	TArray<ATargetPoint*> CurrentWavePodSpawnPoints;

	if(CurrentWavePodCount > PodSpawnPoints.Num())
	{
		CurrentWavePodCount = PodSpawnPoints.Num();
		CurrentWavePodSpawnPoints = PodSpawnPoints;
	}else
	{
		for (int i = 0; i < PodSpawnPoints.Num(); ++i)
		{
			if(FMath::RandHelper(PodSpawnPoints.Num() - i - 1) < CurrentWavePodCount - CurrentWavePodSpawnPoints.Num())
			{
				CurrentWavePodSpawnPoints.Add(PodSpawnPoints[i]);
				if (CurrentWavePodSpawnPoints.Num() == CurrentWavePodCount) break;
			}
		}
	}

	for (auto Point : CurrentWavePodSpawnPoints)
	{
		auto NewPod = Cast<ATDSEnemySpawnPod>(GetWorld()->SpawnActor(EnemySpawnPodClass, &Point->GetActorTransform()));
		EnemySpawnPods.Add(NewPod);
	}
	
	EnemyCountLeftInWave = EnemyCountInCurrentWave;
	EnemySquadSpawnedInWaveCount = 0;

	GetSurvivalGameState()->SetEnemyCountLeftInWave(EnemyCountLeftInWave);
	GetSurvivalGameState()->SetEnemyCountSpawnedInWave(0);
	GetSurvivalGameState()->SetCurrentWave(CurrentWave);
	
	GetWorldTimerManager().SetTimer(EnemySpawnTimerHandle,
		this,
		&ATDSSurvivalGameMode::SpawnEnemy,
		1,
		true,
		WaveStartFirstSpawnDelay);
	SetGamePhase(ESurvivalGamePhase::WaveInBound);
}

void ATDSSurvivalGameMode::EndWave()
{
	for (auto EnemySpawnPod : EnemySpawnPods)
	{
		EnemySpawnPod->Retreat();
	}

	EnemySpawnPods.Empty();
	
	GetWorldTimerManager().SetTimer(WaveBreakTimerHandle, this, &ATDSSurvivalGameMode::StartWave, WaveBreakDuration);
	GetWorldTimerManager().SetTimer(WaveBreakNotifyTimerHandle, this,
		&ATDSSurvivalGameMode::OnWaveBreakTimerUpdate, .25f, true, 0);
	SetGamePhase(ESurvivalGamePhase::WaveBreak);
}

void ATDSSurvivalGameMode::GameOver()
{
	if (WaveBreakTimerHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(WaveBreakTimerHandle);
	}
	SetGamePhase(ESurvivalGamePhase::Ended);
}

void ATDSSurvivalGameMode::SkipBreak()
{
	if(WaveBreakTimerHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(WaveBreakTimerHandle);
		OnWaveBreakTimerUpdate();
		GetWorldTimerManager().ClearTimer(WaveBreakNotifyTimerHandle);
		StartWave();
	}
}

void ATDSSurvivalGameMode::SpawnEnemy()
{
	if (EnemySpawnPods.Num() == 0 || CurrentWaveSquads.Num() == 0) {
		UE_LOG(LogTemp, Warning, TEXT("Can't spawn enemy; no pod found or no squads!"));
		return;
	}
	
	auto SpawnedEnemies = 
		EnemySpawnPods[FMath::RandHelper(EnemySpawnPods.Num())]
		->SpawnSquad(CurrentWaveSquads[EnemySquadSpawnedInWaveCount].Data);

	for (auto SpawnedEnemy : SpawnedEnemies)
	{
		BindToEnemyEndPlay(SpawnedEnemy);
	}
	
	EnemySquadSpawnedInWaveCount++;
	if(EnemySquadSpawnedInWaveCount >= CurrentWaveSquads.Num())
	{
		GetWorldTimerManager().ClearTimer(EnemySpawnTimerHandle);
	}
}

void ATDSSurvivalGameMode::BindToEnemyEndPlay(AActor* Actor)
{
	if (!Actor) return;
	Actor->OnEndPlay.AddDynamic(this, &ATDSSurvivalGameMode::OnSpawnedEnemyEndPlay);
}

void ATDSSurvivalGameMode::OnSpawnedEnemyEndPlay(AActor* Actor, EEndPlayReason::Type Reason)
{
	TotalEnemyKilled++;
	EnemyCountLeftInWave--;
	GetSurvivalGameState()->SetEnemyCountLeftInWave(EnemyCountLeftInWave);
	GetSurvivalGameState()->AddCredits(CreditBountyPerEnemy);
	if(EnemyCountLeftInWave <= 0)
	{
		EndWave();
	}
}

void ATDSSurvivalGameMode::SetGamePhase(ESurvivalGamePhase NewPhase)
{
	GetSurvivalGameState()->SetGamePhase(NewPhase);
}

void ATDSSurvivalGameMode::OnWaveBreakTimerUpdate()
{
	GetSurvivalGameState()->SetWaveBreakTimeRemaining(GetWaveBreakDurationLeft());
}
