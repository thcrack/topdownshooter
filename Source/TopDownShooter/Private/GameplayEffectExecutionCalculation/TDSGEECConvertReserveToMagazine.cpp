// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGEECConvertReserveToMagazine.h"
#include "AbilitySystemComponent.h"
#include "TDSWeaponAttributeSet.h"
#include "TDSAttributeSet.h"

void UTDSGEECConvertReserveToMagazine::Execute_Implementation(
	const FGameplayEffectCustomExecutionParameters& ExecutionParams,
	FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	auto TargetASC = ExecutionParams.GetTargetAbilitySystemComponent();
	auto AmmoReserveAttribute = UTDSAttributeSet::GetAmmoReserveAttribute();
	auto AmmoInMagAttribute = UTDSWeaponAttributeSet::GetAmmoInMagazineAttribute();
	auto AmmoReserve = TargetASC->GetNumericAttribute(AmmoReserveAttribute);
	auto MagSize = TargetASC->GetNumericAttribute(UTDSWeaponAttributeSet::GetMagazineSizeAttribute());
	auto AmmoCost = TargetASC->GetNumericAttribute(UTDSWeaponAttributeSet::GetAmmoReserveCostAttribute());

	auto FullCost = MagSize * AmmoCost;
	if(AmmoReserve >= FullCost)
	{
		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(
			AmmoInMagAttribute,
			EGameplayModOp::Override,
			MagSize
		));

		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(
			AmmoReserveAttribute,
			EGameplayModOp::Additive,
			-FullCost
		));
	}else
	{
		auto AmmoCount = FMath::FloorToFloat(AmmoReserve / AmmoCost);
		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(
			AmmoInMagAttribute,
			EGameplayModOp::Override,
			AmmoCount
		));

		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(
			AmmoReserveAttribute,
			EGameplayModOp::Additive,
			-AmmoCount * AmmoCost
		));
	}
}
