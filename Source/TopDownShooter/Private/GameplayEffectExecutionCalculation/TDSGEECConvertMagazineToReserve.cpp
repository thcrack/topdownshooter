// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGEECConvertMagazineToReserve.h"
#include "AbilitySystemComponent.h"
#include "TDSWeaponAttributeSet.h"
#include "TDSAttributeSet.h"

UTDSGEECConvertMagazineToReserve::UTDSGEECConvertMagazineToReserve()
{
}

void UTDSGEECConvertMagazineToReserve::Execute_Implementation(
	const FGameplayEffectCustomExecutionParameters& ExecutionParams,
	FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	auto TargetASC = ExecutionParams.GetTargetAbilitySystemComponent();
	auto AmmoInMagAttribute = UTDSWeaponAttributeSet::GetAmmoInMagazineAttribute();
	auto AmmoLeftInWeapon = TargetASC->GetNumericAttribute(AmmoInMagAttribute);
	auto AmmoCost = TargetASC->GetNumericAttribute(UTDSWeaponAttributeSet::GetAmmoReserveCostAttribute());

	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(
		UTDSAttributeSet::GetAmmoReserveAttribute(),
		EGameplayModOp::Additive,
		AmmoLeftInWeapon * AmmoCost
	));

	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(
		AmmoInMagAttribute,
		EGameplayModOp::Override,
		0
	));
}
