// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSAttributeSet.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"
#include "TDSFactionInterface.h"
#include "TDSDamageCauserCallbackInterface.h"

UTDSAttributeSet::UTDSAttributeSet()
{
	energyDamageTypeTag = FGameplayTag::RequestGameplayTag(FName("DamageType.Energy"));
	physicalDamageTypeTag = FGameplayTag::RequestGameplayTag(FName("DamageType.Physical"));
}

void UTDSAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);

	if(Attribute == GetSlowPercentageAttribute())
	{
		if(auto ASC = GetOwningAbilitySystemComponent())
		{
			FGameplayEffectQuery Q;
			Q.ModifyingAttribute = GetSlowPercentageAttribute();
			auto SlowEffects = ASC->GetActiveEffects(Q);
			LargestSlowEffectPercentage = 0;
			for(auto& Effect : SlowEffects)
			{
				auto& Spec = ASC->GetActiveGameplayEffect(Effect)->Spec;
				float MSMag = 0;
				
				for(int Idx = 0; Idx < Spec.Def->Modifiers.Num(); Idx++)
				{
					auto& Mod = Spec.Def->Modifiers[Idx];
					if(Mod.Attribute == GetSlowPercentageAttribute())
					{
						MSMag += Spec.GetModifierMagnitude(Idx, true);
					}
				}
				
				LargestSlowEffectPercentage = FMath::Max(LargestSlowEffectPercentage, MSMag);
			}
			SetCalculatedMaxSpeed(GetMaxSpeed() * (1.0f - LargestSlowEffectPercentage));
		}
	}
	else if(Attribute == GetMaxSpeedAttribute())
	{
		SetCalculatedMaxSpeed(NewValue * (1.0f - LargestSlowEffectPercentage));
	}
}

bool UTDSAttributeSet::PreGameplayEffectExecute(FGameplayEffectModCallbackData& Data)
{
	return true;
}

void UTDSAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	if(Data.EvaluatedData.Attribute == GetDamageAttribute())
	{
		FGameplayTagContainer Container;
		Data.EffectSpec.GetAllAssetTags(Container);

		//UE_LOG(LogTemp, Warning, TEXT("Attribute Damage: %f"), GetDamage());
		
		auto CurrentArmor = GetArmor();
		auto CurrentHealth = GetHealth();
		auto DealtDamage = GetDamage();
		auto ArmorDamageMultiplier = 1.0f;
		auto HealthDamageMultiplier = 1.0f;

		auto FactionMultiplier = 1.0f;
		auto InstigatorActor = Data.EffectSpec.GetEffectContext().GetInstigator();
		auto TargetActor = GetOwningAbilitySystemComponent()->GetOwner();
		if(InstigatorActor && TargetActor
			&& Cast<ITDSFactionInterface>(InstigatorActor)
			&& Cast<ITDSFactionInterface>(TargetActor))
		{
				auto InstigatorFaction = ITDSFactionInterface::Execute_GetFaction(InstigatorActor);
				if (InstigatorFaction == ITDSFactionInterface::Execute_GetFaction(TargetActor))
				{
					switch (InstigatorFaction)
					{
					case ETDSFaction::None:		FactionMultiplier = 1.0f;  break;
					case ETDSFaction::GoodGuys:
						FactionMultiplier = InstigatorActor == TargetActor ? 0.5f : 0.1f;
						break;
					case ETDSFaction::BadGuys:	FactionMultiplier = 1.0f; break;
					case ETDSFaction::Team1:	FactionMultiplier = 0.1f; break;
					case ETDSFaction::Team2:	FactionMultiplier = 0.1f; break;
					default:;
					}
				}
		}

		DealtDamage *= FactionMultiplier;
		
		if (Container.HasTagExact(energyDamageTypeTag))
		{
			ArmorDamageMultiplier = 0.5f;
			HealthDamageMultiplier = 2.0f;
			
		}

		auto ActualDamage = 0.f;
		auto ArmorDamage = DealtDamage * ArmorDamageMultiplier;

		if (CurrentArmor >= ArmorDamage)
		{
			CurrentArmor = CurrentArmor - ArmorDamage;
			ActualDamage = ArmorDamage;
		}
		else if (CurrentArmor > 0)
		{
			float HealthDamageLeft = (DealtDamage - CurrentArmor / ArmorDamageMultiplier) * HealthDamageMultiplier;
			ActualDamage += CurrentArmor;
			ActualDamage += HealthDamageLeft;
			CurrentArmor = 0;
			CurrentHealth = CurrentHealth - HealthDamageLeft;
		}
		else
		{
			ActualDamage = DealtDamage * HealthDamageMultiplier;
			CurrentHealth = CurrentHealth - ActualDamage;
		}

		auto Causer = Data.EffectSpec.GetEffectContext().GetEffectCauser();
		if(Causer->GetClass()->ImplementsInterface(UTDSDamageCauserCallbackInterface::StaticClass()))
		{
			ITDSDamageCauserCallbackInterface::Execute_CausedDamageCallback(
				Causer,
				ActualDamage,
				Data.EffectSpec.GetEffectContext());
		}
		
		auto Instigator = Data.EffectSpec.GetEffectContext().GetInstigator();
		if (Instigator->GetClass()->ImplementsInterface(UTDSDamageCauserCallbackInterface::StaticClass()))
		{
			ITDSDamageCauserCallbackInterface::Execute_CausedDamageCallback(
				Instigator,
				ActualDamage,
				Data.EffectSpec.GetEffectContext());
		}
		//Data.EffectSpec.GetEffectContext().Get

		// !!! Called before setting health to prevent destroying actor making a memory violation
		OnAttributeSetReceiveDamage.Broadcast(ActualDamage, Data.EffectSpec.GetEffectContext());

		SetDamage(0);
		SetArmor(FMath::Clamp(CurrentArmor, .0f, GetMaxArmor()));
		SetHealth(FMath::Clamp(CurrentHealth, .0f, GetMaxHealth()));
	}
	else if (Data.EvaluatedData.Attribute == GetAmmoReserveAttribute())
	{
		SetAmmoReserve(FMath::Clamp(GetAmmoReserve(), .0f, GetMaxAmmoReserve()));
	}
	else if (Data.EvaluatedData.Attribute == GetArmorAttribute())
	{
		SetArmor(FMath::Clamp(GetArmor(), .0f, GetMaxArmor()));
	}
	else if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		SetHealth(FMath::Clamp(GetHealth(), .0f, GetMaxHealth()));
	}else if (Data.EvaluatedData.Attribute == GetDashChargeAttribute())
	{
		SetDashCharge(FMath::Clamp(GetDashCharge(), .0f, 100.f));
	}
}

void UTDSAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void UTDSAttributeSet::InitFromMetaDataTable(const UDataTable* DataTable)
{
	Super::InitFromMetaDataTable(DataTable);
	SetCalculatedMaxSpeed(GetMaxSpeed());
}
