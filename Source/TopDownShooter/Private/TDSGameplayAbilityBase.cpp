// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameplayAbilityBase.h"
#include "AbilitySystemComponent.h"
#include "Engine/CompositeCurveTable.h"

UTDSGameplayAbilityBase::UTDSGameplayAbilityBase()
{
	CooldownSetByCallerTag = FGameplayTag::RequestGameplayTag(FName("Cooldown"));
	bInheritCooldownTags = false;
	//StatTable->FindCurve(FName())->Eval()
}

UCompositeCurveTable* UTDSGameplayAbilityBase::GetStatTable() const
{
	return StatTable;
}

FGameplayTagContainer* UTDSGameplayAbilityBase::GetCooldownTags() const
{
	if (!bInheritCooldownTags) return new FGameplayTagContainer(CooldownTags);
	
	FGameplayTagContainer* Tags = new FGameplayTagContainer();
	const FGameplayTagContainer* ParentTags = Super::GetCooldownTags();
	if (ParentTags)
	{
		Tags->AppendTags(*ParentTags);
	}
	Tags->AppendTags(CooldownTags);
	return Tags;
}

void UTDSGameplayAbilityBase::ApplyCooldown(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) const
{
	UGameplayEffect* CooldownGE = GetCooldownGameplayEffect();
	if (CooldownGE)
	{
		FGameplayEffectSpecHandle SpecHandle = MakeOutgoingGameplayEffectSpec(CooldownGE->GetClass(), GetAbilityLevel());
		SpecHandle.Data.Get()->DynamicGrantedTags.AppendTags(CooldownTags);
		SpecHandle.Data.Get()->SetSetByCallerMagnitude(CooldownSetByCallerTag, CooldownDuration.GetValueAtLevel(GetAbilityLevel()));
		ApplyGameplayEffectSpecToOwner(Handle, ActorInfo, ActivationInfo, SpecHandle);
	}
}

bool UTDSGameplayAbilityBase::CheckCost(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, FGameplayTagContainer* OptionalRelevantTags) const
{
	if(bIgnoreCostForActivation)
	{
		auto Spec = ActorInfo->AbilitySystemComponent->FindAbilitySpecFromHandle(Handle);
		if (Spec && !Spec->IsActive()) return true;
	}
	return Super::CheckCost(Handle, ActorInfo, OptionalRelevantTags);
}

void UTDSGameplayAbilityBase::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	Super::OnAvatarSet(ActorInfo, Spec);

	if (ActivateAbilityOnGranted)
	{
		bool ActivatedAbility = ActorInfo->AbilitySystemComponent->TryActivateAbility(Spec.Handle, false);
	}
}
