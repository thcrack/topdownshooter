// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameplayAbilityTargetActor.h"
#include "GameplayAbility.h"
#include "AbilitySystemComponent.h"

void ATDSGameplayAbilityTargetActor::ConfirmTargeting()
{
	// Check is performed twice, can be improved
	if (IsConfirmTargetingAllowed())
	{
		ConfirmTargetingAndContinue();
		
		if (bDestroyOnConfirmation)
		{
			const FGameplayAbilityActorInfo* ActorInfo = (OwningAbility ? OwningAbility->GetCurrentActorInfo() : nullptr);
			UAbilitySystemComponent* ASC = (ActorInfo ? ActorInfo->AbilitySystemComponent.Get() : nullptr);
			if (ASC)
			{
				ASC->AbilityReplicatedEventDelegate(EAbilityGenericReplicatedEvent::GenericConfirm, OwningAbility->GetCurrentAbilitySpecHandle(), OwningAbility->GetCurrentActivationInfo().GetActivationPredictionKey()).Remove(GenericConfirmHandle);
			}
			else
			{
				ABILITY_LOG(Warning, TEXT("AGameplayAbilityTargetActor::ConfirmTargeting called with null Ability/ASC! Actor %s"), *GetName());
			}
			
			Destroy();
		}
	}
}

void ATDSGameplayAbilityTargetActor::ConfirmTargetingAndContinue()
{
	check(ShouldProduceTargetData());
	
	if (IsConfirmTargetingAllowed())
	{
		bDebug = false;
		TargetDataReadyDelegate.Broadcast(MakeTargetDataHandle());
	}
}

bool ATDSGameplayAbilityTargetActor::CanBeConfirmed_Implementation()
{
	return true;
}

bool ATDSGameplayAbilityTargetActor::IsConfirmTargetingAllowed()
{
	return Super::IsConfirmTargetingAllowed() && CanBeConfirmed();
}
