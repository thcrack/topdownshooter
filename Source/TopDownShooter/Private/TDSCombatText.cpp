// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSCombatText.h"
#include "WidgetComponent.h"

// Sets default values
ATDSCombatText::ATDSCombatText()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bLaunched = false;

	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget Component"));
	SetRootComponent(WidgetComponent);
}

// Called when the game starts or when spawned
void ATDSCombatText::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDSCombatText::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDSCombatText::SetText(const FText& InText)
{
	Text = InText;
	OnSetText(Text);
}

void ATDSCombatText::Launch()
{
	if (bLaunched) return;

	bLaunched = true;

	OnLaunch();
}

