// Fill out your copyright notice in the Description page of Project Settings.

#include "TDSKiosk.h"
#include "Engine/DataTable.h"
#include "Equipment/TDSArmorEquipment.h"
#include "TDSWeapon.h"
#include "TDSWeaponEquipment.h"
#include "TDSAbilityEquipment.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "TDSPlayerCharacter.h"
#include "TimerManager.h"
#include "TDSSurvivalGameState.h"
#include "TDSSurvivalPlayerState.h"

// Sets default values
ATDSKiosk::ATDSKiosk()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TimeBeforeOpeningUI = 1.5f;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SetRootComponent(MeshComponent);
	
	PlayerDetectionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("PlayerDetectionSphere"));
	PlayerDetectionSphere->SetupAttachment(MeshComponent);
	PlayerDetectionSphere->ResetRelativeTransform();
	PlayerDetectionSphere->SetSphereRadius(256.0f);

	ContextString = FString(TEXT("Kiosk"));

	PlayerDetectionSphere->SetVisibility(false);
	PlayerDetectionSphere->SetCollisionProfileName(TEXT("NoCollision"));

	bKioskEnabled = false;

}

// Called when the game starts or when spawned
void ATDSKiosk::BeginPlay()
{
	Super::BeginPlay();

	PlayerDetectionSphere->OnComponentBeginOverlap.AddDynamic(this, &ATDSKiosk::OnActorEntersDetectionSphere);
	PlayerDetectionSphere->OnComponentEndOverlap.AddDynamic(this, &ATDSKiosk::OnActorLeavesDetectionSphere);

	FillSellingItems();
	
}

int ATDSKiosk::GetCurrentPlayersInRangeCount(bool bUseComponent)
{
	if (!bKioskEnabled) return 0;

	if(bUseComponent)
	{
		TArray<AActor*> OverlappingActors;
		PlayerDetectionSphere->GetOverlappingActors(OverlappingActors, ATDSPlayerCharacter::StaticClass());
		
		return OverlappingActors.Num();
	}
	
	const TArray<AActor*> IgnoreActors;
	TArray<AActor*> OutActors;
	const TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
	UKismetSystemLibrary::SphereOverlapActors(
		GetWorld(),
		PlayerDetectionSphere->GetComponentLocation(),
		PlayerDetectionSphere->GetScaledSphereRadius(),
		ObjectTypes,
		ATDSPlayerCharacter::StaticClass(),
		IgnoreActors,
		OutActors
	);
	
	return OutActors.Num();
}

void ATDSKiosk::EnableKiosk()
{
	if (bKioskEnabled) return;
	
	bKioskEnabled = true;

	PlayerDetectionSphere->SetVisibility(true);
	PlayerDetectionSphere->SetCollisionProfileName(TEXT("OverlapAll"));
	
	if (GetCurrentPlayersInRangeCount(true) >= PlayersRequiredToOpen)
	{
		GetWorldTimerManager().SetTimer(OpenUITimerHandle, this, &ATDSKiosk::OnOpenUI_Internal, TimeBeforeOpeningUI);
	}
	
	OnKioskEnabled();
}

void ATDSKiosk::DisableKiosk()
{
	if (!bKioskEnabled) return;
	
	bKioskEnabled = false;
	
	PlayerDetectionSphere->SetVisibility(false);
	PlayerDetectionSphere->SetCollisionProfileName(TEXT("NoCollision"));

	if(OpenUITimerHandle.IsValid()) GetWorldTimerManager().ClearTimer(OpenUITimerHandle);
	
	OnKioskDisabled();
}

void ATDSKiosk::AttemptOpenKiosk()
{
	if(GetCurrentPlayersInRangeCount(true) >= PlayersRequiredToOpen)
	{
		GetWorldTimerManager().SetTimer(OpenUITimerHandle, this, &ATDSKiosk::OnOpenUI_Internal, TimeBeforeOpeningUI);
	}
}

void ATDSKiosk::OnActorEntersDetectionSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{	
	ATDSPlayerCharacter* PlayerCharacter = Cast<ATDSPlayerCharacter>(OtherActor);
	if(PlayerCharacter)
	{
		AttemptOpenKiosk();
	}
}

void ATDSKiosk::OnActorLeavesDetectionSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ATDSPlayerCharacter* PlayerCharacter = Cast<ATDSPlayerCharacter>(OtherActor);
	if (PlayerCharacter)
	{
		if (GetCurrentPlayersInRangeCount(true) < PlayersRequiredToOpen)
		{
			GetWorldTimerManager().ClearTimer(OpenUITimerHandle);
		}
	}
}

void ATDSKiosk::OnOpenUI_Internal()
{
	OnOpenUI();
}

void ATDSKiosk::Restock()
{
	for (auto i = 0; i < SellingItems.Num(); ++i)
	{
		if(SellingItems[i] == nullptr)
		{
			auto EquipmentType = FMath::RandHelper(3);
			if (EquipmentType == 0)
			{
				auto WeaponTableRowNames = WeaponTable->GetRowNames();
				const auto WeaponData = WeaponTable->FindRow<FWeaponData>(WeaponTableRowNames[FMath::RandHelper(WeaponTableRowNames.Num())], ContextString);
				SellingItems[i] = UTDSWeaponEquipment::MakeWeaponEquipment(*WeaponData);
			}
			else if (EquipmentType == 1) {
				auto ArmorTableRowNames = ArmorTable->GetRowNames();
				const auto ArmorData = ArmorTable->FindRow<FArmorData>(ArmorTableRowNames[FMath::RandHelper(ArmorTableRowNames.Num())], ContextString);
				SellingItems[i] = UTDSArmorEquipment::MakeArmorEquipment(*ArmorData);
			}else{
				auto AbilityTableRowNames = AbilityTable->GetRowNames();
				const auto AbilityData = AbilityTable->FindRow<FAbilityData>(AbilityTableRowNames[FMath::RandHelper(AbilityTableRowNames.Num())], ContextString);
				SellingItems[i] = UTDSAbilityEquipment::MakeAbilityEquipment(*AbilityData, 0);
			}
		}
	}
}

void ATDSKiosk::FillSellingItems()
{

	SellingItems.Empty();
	SellingItems.Reserve(SellingItemsMaxCount);

	auto WeaponTableRowNames = WeaponTable->GetRowNames();
	for (auto i = 0; i < 4; ++i)
	{
		const auto WeaponData = WeaponTable->FindRow<FWeaponData>(WeaponTableRowNames[FMath::RandHelper(WeaponTableRowNames.Num())], ContextString);
		SellingItems.Add(UTDSWeaponEquipment::MakeWeaponEquipment(*WeaponData));
	}
	
	auto ArmorTableRowNames = ArmorTable->GetRowNames();
	for (auto i = 0; i < 4; ++i)
	{
		const auto ArmorData = ArmorTable->FindRow<FArmorData>(ArmorTableRowNames[FMath::RandHelper(ArmorTableRowNames.Num())], ContextString);
		SellingItems.Add(UTDSArmorEquipment::MakeArmorEquipment(*ArmorData));
	}
	
	auto AbilityTableRowNames = AbilityTable->GetRowNames();
	for (auto i = 0; i < 8; ++i)
	{
		const auto AbilityData = AbilityTable->FindRow<FAbilityData>(AbilityTableRowNames[FMath::RandHelper(AbilityTableRowNames.Num())], ContextString);
		SellingItems.Add(UTDSAbilityEquipment::MakeAbilityEquipment(*AbilityData, 0));
	}
}

void ATDSKiosk::RefreshAllSellingItems()
{
	SellingItems.Empty();
	FillSellingItems();
	for (int i = 0; i < SellingItems.Num(); i++)
	{
		OnSellingItemsChanged.Broadcast(i, SellingItems[i]);
	}
}

bool ATDSKiosk::CanPurchaseSellingItem(int Index, int PlayerId)
{
	if (Index >= SellingItems.Num())
	{
		UE_LOG(LogTemp, Warning, TEXT("Purchase index out of bound!"));
		return false;
	}

	if (!SellingItems[Index])
	{
		UE_LOG(LogTemp, Warning, TEXT("No item at the index!"));
		return false;
	}

	auto GameState = Cast<ATDSSurvivalGameState>(GetWorld()->GetGameState());
	if (!GameState) return false;

	if (GameState->PlayerArray.Num() <= PlayerId)
	{
		UE_LOG(LogTemp, Warning, TEXT("Player index out of bound!"));
		return false;
	}
	
	auto PlayerState = Cast<ATDSSurvivalPlayerState>(GameState->PlayerArray[PlayerId]);
	if(!PlayerState)
	{
		UE_LOG(LogTemp, Warning, TEXT("Can't cast PlayerState!"));
		return false;
	}

	if(PlayerState->GetCredits() < SellingItems[Index]->GetPrice())
	{
		UE_LOG(LogTemp, Warning, TEXT("Insufficient credits!"));
		return false;
	}
	
	return true;
}

UTDSEquipment* ATDSKiosk::PurchaseSellingItem(int Index, int PlayerId)
{
	return CanPurchaseSellingItem(Index, PlayerId) ? PurchaseSellingItemChecked(Index, PlayerId) : nullptr;
}

UTDSEquipment* ATDSKiosk::PurchaseSellingItemChecked(int Index, int PlayerId)
{
	auto Temp = SellingItems[Index];

	SellingItems[Index] = nullptr;

	Cast<ATDSSurvivalPlayerState>(
			GetWorld()->GetGameState()->PlayerArray[PlayerId]
		)->DeductCredits(Temp->GetPrice());

	OnSellingItemsChanged.Broadcast(Index, SellingItems[Index]);

	return Temp;
}

UTDSEquipment* ATDSKiosk::PurchaseSellingItemWithEquipment(UTDSEquipment* Equipment, int PlayerId)
{
	auto Index = SellingItems.Find(Equipment);
	
	if(Index == INDEX_NONE)
	{
		UE_LOG(LogTemp, Warning, TEXT("Purchse equipment not found!"));
		return nullptr;
	}

	return PurchaseSellingItem(Index, PlayerId);
}

bool ATDSKiosk::PurchaseSellingItemAndGiveToPlayerCharacter(int Index, ATDSPlayerCharacter* PlayerCharacter, EExistingEquipmentPolicy ExistingEquipmentPolicy)
{
	auto CanPurchase = CanPurchaseSellingItem(Index, PlayerCharacter->PlayerId);
	if(CanPurchase)
	{
		auto Item = SellingItems[Index];
		auto GameState = Cast<ATDSSurvivalGameState>(
			GetWorld()->GetGameState()
			);
		switch (Item->GetType()) {
			case EEquipmentType::EET_Weapon:
				UTDSWeaponEquipment* ExistingWeapon;
				if(PlayerCharacter->UnequipWeapon(ExistingWeapon, true))
				{
					switch (ExistingEquipmentPolicy) {
						case EExistingEquipmentPolicy::EEEP_Sell:
							GameState->AddCredits(ExistingWeapon->GetPrice() / 2);
							break;
						
						case EExistingEquipmentPolicy::EEEP_ToStorage:
						// Ignore cases without available slot because it should be checked beforehand
							GameState->PutEquipmentToEmptyStorageSlot(ExistingWeapon);
							break;
					}
				}
				CanPurchase = PlayerCharacter->EquipWeapon(Cast<UTDSWeaponEquipment>(Item));
			break;
			
			case EEquipmentType::EET_Armor:
				UTDSArmorEquipment* ExistingArmor;
				if (PlayerCharacter->UnequipArmor(ExistingArmor, true))
				{
					switch (ExistingEquipmentPolicy) {
						case EExistingEquipmentPolicy::EEEP_Sell:
							GameState->AddCredits(ExistingArmor->GetPrice() / 2);
							break;
						case EExistingEquipmentPolicy::EEEP_ToStorage:
							// Ignore cases without available slot because it should be checked beforehand
							GameState->PutEquipmentToEmptyStorageSlot(ExistingArmor);
							break;
					}
				}
				CanPurchase = PlayerCharacter->EquipArmor(Cast<UTDSArmorEquipment>(Item));
			break;
			
			case EEquipmentType::EET_Ability:
				CanPurchase = PlayerCharacter->EquipAbility(Cast<UTDSAbilityEquipment>(Item));
			break;
		}
	}

	if(CanPurchase)
	{
		PurchaseSellingItemChecked(Index, PlayerCharacter->PlayerId);
	}
	
	return CanPurchase;
}

bool ATDSKiosk::PurchaseSellingItemAndGiveToStorage(int Index, ATDSPlayerCharacter* PlayerCharacter)
{
	if (!CanPurchaseSellingItem(Index, PlayerCharacter->PlayerId)) return false;
	
	if (!Cast<ATDSSurvivalGameState>(
		GetWorld()->GetGameState()
		)->PutEquipmentToEmptyStorageSlot(SellingItems[Index])) return false;

	PurchaseSellingItemChecked(Index, PlayerCharacter->PlayerId);

	return true;
}
