// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSEquipment.h"

FText UTDSEquipment::MakeFormattedDescription() const
{
	return FText();
}

FText UTDSEquipment::GetName()
{
	return FText();
}

UTexture2D* UTDSEquipment::GetIcon()
{
	return nullptr;
}

float UTDSEquipment::GetPrice()
{
	return 0;
}

EEquipmentRarity UTDSEquipment::GetRarity()
{
	return EEquipmentRarity::EER_Common;
}

EEquipmentType UTDSEquipment::GetType()
{
	return EEquipmentType::EET_None;
}

FText UTDSEquipment::GetFormattedDescription()
{
	if (bHasCachedFormattedDescription) return CachedFormattedDescription;
	
	CachedFormattedDescription = MakeFormattedDescription();
	bHasCachedFormattedDescription = true;
	
	return CachedFormattedDescription;
}

bool UTDSEquipment::InvalidateDescription()
{
	if (!bHasCachedFormattedDescription) return false;

	bHasCachedFormattedDescription = false;
	
	return true;
}
