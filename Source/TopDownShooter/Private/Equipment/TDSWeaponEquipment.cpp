// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSWeaponEquipment.h"

UTDSWeaponEquipment::UTDSWeaponEquipment()
{
}

FText UTDSWeaponEquipment::GetName()
{
	return WeaponData.Name;
}

UTexture2D* UTDSWeaponEquipment::GetIcon()
{
	return WeaponData.Icon;
}

float UTDSWeaponEquipment::GetPrice()
{
	return WeaponData.Price;
}

EEquipmentRarity UTDSWeaponEquipment::GetRarity()
{
	return WeaponData.Rarity;
}

EEquipmentType UTDSWeaponEquipment::GetType()
{
	return EEquipmentType::EET_Weapon;
}

FText UTDSWeaponEquipment::MakeFormattedDescription() const
{
	auto Format = GetWeaponData().Description;

	FFormatNamedArguments Arguments;
	Arguments.Add(TEXT("damage"),				GetWeaponData().Damage);
	Arguments.Add(TEXT("rate_of_fire"),			GetWeaponData().RateOfFire);
	Arguments.Add(TEXT("accuracy"),				GetWeaponData().Accuracy);
	Arguments.Add(TEXT("ammo_reserve_cost"),	GetWeaponData().AmmoReserveCost);
	Arguments.Add(TEXT("magazine_size"),		GetWeaponData().MagazineSize);
	Arguments.Add(TEXT("pellets_per_shot"),		GetWeaponData().PelletsPerShot);
	Arguments.Add(TEXT("reload_time"),			GetWeaponData().ReloadTime);

	return FText::Format(Format, Arguments);
}

UTDSWeaponEquipment* UTDSWeaponEquipment::MakeWeaponEquipment(FWeaponData InWeaponData)
{
	UTDSWeaponEquipment* NewWeaponEquipment = NewObject<UTDSWeaponEquipment>();
	NewWeaponEquipment->WeaponData = InWeaponData;
	NewWeaponEquipment->WeaponDynamicData.AmmoInMagazine = InWeaponData.MagazineSize;
	NewWeaponEquipment->WeaponAbility = InWeaponData.AbilityClass;
	return NewWeaponEquipment;
}

ATDSWeapon* UTDSWeaponEquipment::MakeWeaponActor(UWorld* WorldContext, const FTransform& Transform, const FActorSpawnParameters& SpawnParams)
{

	if (!GetWeaponData().WeaponActorClass) return nullptr;

	return WorldContext->SpawnActor<ATDSWeapon>(GetWeaponData().WeaponActorClass, Transform, SpawnParams);
}

TSubclassOf<UGameplayAbility> UTDSWeaponEquipment::GetWeaponAbility() const
{
	return WeaponAbility;
}

const FWeaponData& UTDSWeaponEquipment::GetWeaponData() const
{
	return WeaponData;
}

const FWeaponDynamicData& UTDSWeaponEquipment::GetWeaponDynamicData() const
{
	return WeaponDynamicData;
}

FWeaponDynamicData& UTDSWeaponEquipment::GetWeaponDynamicDataRef()
{
	return WeaponDynamicData;
}
