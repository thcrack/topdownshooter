// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSAbilityEquipment.h"
#include "TDSGameplayAbilityBase.h"
#include "Engine/CompositeCurveTable.h"

UTDSAbilityEquipment::UTDSAbilityEquipment()
{
}

FText UTDSAbilityEquipment::GetName()
{
	return AbilityData.Name;
}

UTexture2D* UTDSAbilityEquipment::GetIcon()
{
	return AbilityData.Icon;
}

float UTDSAbilityEquipment::GetPrice()
{
	return AbilityData.Price;
}

EEquipmentRarity UTDSAbilityEquipment::GetRarity()
{
	if (AbilityData.AbilityType == EAbilityType::AT_Overdrive) return EEquipmentRarity::EER_Legendary;

	return static_cast<EEquipmentRarity>(AbilityLevel);
}

EEquipmentType UTDSAbilityEquipment::GetType()
{
	return EEquipmentType::EET_Ability;
}

FText UTDSAbilityEquipment::MakeFormattedDescription() const
{
	auto Format = CanUpgrade() ? GetAbilityData().Description : GetAbilityData().MaxLevelDescription;

	auto StatTable = GetAbility().GetDefaultObject()->GetStatTable();
	if (!StatTable) return Format;

	TArray<FString> ParamNames;
	FText::GetFormatPatternParameters(Format, ParamNames);
	if (ParamNames.Num() == 0) return Format;
	
	FFormatNamedArguments Arguments;
	FString Context = TEXT("FormatAbilityDescription");

	for (auto ParamName : ParamNames)
	{
		auto Curve = StatTable->FindCurve(FName(*ParamName), Context);
		if (!Curve) continue;

		if(ParamName.EndsWith(TEXT("_ratio")))
		{
			Arguments.Add(ParamName, FText::AsPercent(Curve->Eval(GetAbilityLevel())));
		}
		else
		{
			Arguments.Add(ParamName, Curve->Eval(GetAbilityLevel()));
		}
	}

	if (Arguments.Num() == 0) return Format;

	return FText::Format(Format, Arguments);
}

bool UTDSAbilityEquipment::CanUpgrade() const
{
	return AbilityData.AbilityType != EAbilityType::AT_Overdrive && AbilityLevel < 3;
}

bool UTDSAbilityEquipment::Upgrade()
{
	if (!CanUpgrade()) return false;

	InvalidateDescription();
	
	AbilityLevel++;
	
	return true;
}

bool UTDSAbilityEquipment::IsSame(const UTDSAbilityEquipment* Other)
{
	return Other->GetAbilityData().Name.EqualTo(GetAbilityData().Name);
}

UTDSAbilityEquipment* UTDSAbilityEquipment::MakeAbilityEquipment(FAbilityData InAbilityData, int InitLevel)
{
	UTDSAbilityEquipment* NewAbilityEquipment = NewObject<UTDSAbilityEquipment>();
	NewAbilityEquipment->AbilityData = InAbilityData;
	NewAbilityEquipment->AbilityLevel = InitLevel;
	return NewAbilityEquipment;
}

FAbilityData UTDSAbilityEquipment::GetAbilityData() const
{
	return AbilityData;
}

TSubclassOf<UTDSGameplayAbilityBase> UTDSAbilityEquipment::GetAbility() const
{
	return AbilityData.AbilityClass;
}

const int UTDSAbilityEquipment::GetAbilityLevel() const
{
	return AbilityLevel;
}
