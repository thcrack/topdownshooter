// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSBlueprintFunctionLibrary.h"
#include "AbilitySystemGlobals.h"

void UTDSBlueprintFunctionLibrary::AddInstigator(FGameplayEffectContextHandle Context, AActor* Instigator,
	AActor* EffectCauser, FGameplayEffectContextHandle& OutContext)
{
	Context.AddInstigator(Instigator, EffectCauser);
	OutContext = Context;
}

FGameplayEffectContextHandle UTDSBlueprintFunctionLibrary::MakeEffectContext(AActor* Instigator, AActor* EffectCauser)
{
	FGameplayEffectContextHandle Context = FGameplayEffectContextHandle(
		UAbilitySystemGlobals::Get().AllocGameplayEffectContext());

	Context.AddInstigator(Instigator, EffectCauser);
	
	return Context;
}

float UTDSBlueprintFunctionLibrary::EvaluateScalableFloat(const FScalableFloat& ScalableFloat, float Level)
{
	return ScalableFloat.GetValueAtLevel(Level);
}

bool UTDSBlueprintFunctionLibrary::IsEffectHandleValid(const FActiveGameplayEffectHandle& ActiveGameplayEffectHandle)
{
	return ActiveGameplayEffectHandle.IsValid();
}
