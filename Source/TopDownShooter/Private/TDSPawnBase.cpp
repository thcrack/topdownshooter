// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSPawnBase.h"
#include "TDSAbilitySystemComponent.h"

// Sets default values
ATDSPawnBase::ATDSPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AbilitySystem = CreateDefaultSubobject<UTDSAbilitySystemComponent>(TEXT("Ability System"));

}

// Called when the game starts or when spawned
void ATDSPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDSPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATDSPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UAbilitySystemComponent* ATDSPawnBase::GetAbilitySystemComponent() const
{
	return AbilitySystem;
}

