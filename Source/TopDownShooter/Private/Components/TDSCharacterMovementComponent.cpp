// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/TDSCharacterMovementComponent.h"
#include "TDSCharacterBase.h"
#include "AbilitySystemComponent.h"
#include "TDSAttributeSet.h"

float UTDSCharacterMovementComponent::GetMaxSpeed() const
{
	ATDSCharacterBase* Owner = Cast<ATDSCharacterBase>(GetOwner());
	if(Owner == nullptr)
	{
		return Super::GetMaxSpeed();
	}

	auto ASC = Owner->GetAbilitySystemComponent();
	if(!ASC) return Super::GetMaxSpeed();
	
	if (!ASC->HasAttributeSetForAttribute(UTDSAttributeSet::GetCalculatedMaxSpeedAttribute()))
	{
		return Super::GetMaxSpeed();
	}
	
	return ASC->GetNumericAttributeChecked(UTDSAttributeSet::GetCalculatedMaxSpeedAttribute());
}
