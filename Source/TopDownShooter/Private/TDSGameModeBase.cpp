// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameModeBase.h"
#include "Engine/World.h"
#include "Engine/GameViewportClient.h"
#include "SGameLayerManager.h"
#include "Widget.h"
#include "TDSCameraAnchor.h"
#include "Kismet/GameplayStatics.h"

ATDSGameModeBase::ATDSGameModeBase(): Super()
{
	PrimaryActorTick.bCanEverTick = true;
	CameraAnchorClass = ATDSCameraAnchor::StaticClass();
}

void ATDSGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	CameraAnchor = Cast<ATDSCameraAnchor>(GetWorld()->SpawnActor(CameraAnchorClass));

	UGameplayStatics::GetPlayerCameraManager(this, 0)->SetViewTarget(CameraAnchor);
	WatchDefaultActors();
}

void ATDSGameModeBase::WatchDefaultActors()
{
	// Watch all player-controlled pawns
	UGameplayStatics::GetAllActorsOfClass(this, DefaultPawnClass, ImportantActors);
	UE_LOG(LogTemp, Warning, TEXT("Watched %d default actors"), ImportantActors.Num());
}

void ATDSGameModeBase::AddActorToWatch(AActor* Actor)
{
	ImportantActors.AddUnique(Actor);
}

void ATDSGameModeBase::RemoveActorToWatch(AActor* Actor)
{
	ImportantActors.RemoveSwap(Actor);
}

void ATDSGameModeBase::SetWidgetLayerVisibility(FName LayerName, ESlateVisibility Visibility)
{
	auto LocalWorld = GetWorld();
	if (!LocalWorld) return;
	
	auto Viewport = GetWorld()->GetGameViewport();
	if (!Viewport) return;
	
	auto LayerManager = Viewport->GetGameLayerManager();
	if (!LayerManager.IsValid()) return;
	
	UGameInstance* GameInstance = LocalWorld->GetGameInstance();
	check(GameInstance);

	auto Player = GameInstance->GetFirstGamePlayer();
	auto Layer = LayerManager->FindLayerForPlayer(Player, LayerName);
	if (!Layer.IsValid()) return;

	auto LayerWidget = Layer->AsWidget();
	LayerWidget.Get().SetVisibility(UWidget::ConvertSerializedVisibilityToRuntime(Visibility));
}

void ATDSGameModeBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	CameraAnchor->UpdateAnchor(ImportantActors, DeltaSeconds);
}

void ATDSGameModeBase::SetViewTargetWithBlend(AActor* NewViewTarget, float BlendTime, EViewTargetBlendFunction BlendFunc, float BlendExp, bool bLockOutgoing)
{
	FViewTargetTransitionParams TransitionParams;
	TransitionParams.BlendTime = BlendTime;
	TransitionParams.BlendFunction = BlendFunc;
	TransitionParams.BlendExp = BlendExp;
	TransitionParams.bLockOutgoing = bLockOutgoing;

	UGameplayStatics::GetPlayerCameraManager(this, 0)->SetViewTarget(NewViewTarget, TransitionParams);
}
