// Copyright 2019 Dan Kestranek.

#include "AsyncTask/AsyncTaskCooldownChanged.h"
#include "TimerManager.h"
#include "GameplayTagContainer.h"

UAsyncTaskCooldownChanged* UAsyncTaskCooldownChanged::ListenForCooldownChange(UAbilitySystemComponent* AbilitySystemComponent, FGameplayTag InCooldownTag, bool InUseServerCooldown, bool InEndTaskOnCooldownEnded, float InActiveTimeUpdateBroadcastInterval)
{
	UAsyncTaskCooldownChanged* ListenForCooldownChange = NewObject<UAsyncTaskCooldownChanged>();
	ListenForCooldownChange->ASC = AbilitySystemComponent;
	ListenForCooldownChange->World = AbilitySystemComponent->GetWorld();
	ListenForCooldownChange->CooldownTag = InCooldownTag;
	ListenForCooldownChange->UseServerCooldown = InUseServerCooldown;
	ListenForCooldownChange->bEndTaskOnCooldownEnded = InEndTaskOnCooldownEnded;
	ListenForCooldownChange->ActiveTimeUpdateBroadcastInterval = InActiveTimeUpdateBroadcastInterval;

	if (!IsValid(AbilitySystemComponent))
	{
		ListenForCooldownChange->RemoveFromRoot();
		return nullptr;
	}

	AbilitySystemComponent->OnActiveGameplayEffectAddedDelegateToSelf.AddUObject(ListenForCooldownChange, &UAsyncTaskCooldownChanged::OnActiveGameplayEffectAddedCallback);

	AbilitySystemComponent->RegisterGameplayTagEvent(InCooldownTag, EGameplayTagEventType::NewOrRemoved).AddUObject(ListenForCooldownChange, &UAsyncTaskCooldownChanged::CooldownTagChanged);

	float TimeRemaining = 0.0f;
	float Duration = 0.0f;
	if (ListenForCooldownChange->GetCooldownRemainingForTag(InCooldownTag, TimeRemaining, Duration))
	{
		ListenForCooldownChange->StartCooldownUpdate(InCooldownTag, TimeRemaining, Duration);
	}

	// ListenForCooldownChange->OnCooldownBegin.AddDynamic(ListenForCooldownChange, &UAsyncTaskCooldownChanged::StartCooldownUpdate);
	// ListenForCooldownChange->OnCooldownEnd.AddDynamic(ListenForCooldownChange, &UAsyncTaskCooldownChanged::StopCooldownUpdate);
	
	return ListenForCooldownChange;
}

void UAsyncTaskCooldownChanged::UnregisterEvents()
{
	if (IsValid(ASC))
	{
		ASC->OnActiveGameplayEffectAddedDelegateToSelf.RemoveAll(this);

		ASC->RegisterGameplayTagEvent(CooldownTag, EGameplayTagEventType::NewOrRemoved).RemoveAll(this);
	}

	if(UpdateTimerHandle.IsValid())
	{
		World->GetTimerManager().ClearTimer(UpdateTimerHandle);
		UpdateTimerHandle.Invalidate();
	}
}

void UAsyncTaskCooldownChanged::BeginDestroy()
{
	UnregisterEvents();

	Super::BeginDestroy();
}

void UAsyncTaskCooldownChanged::OnActiveGameplayEffectAddedCallback(UAbilitySystemComponent* Target, const FGameplayEffectSpec& SpecApplied, FActiveGameplayEffectHandle ActiveHandle)
{
	FGameplayTagContainer GrantedTags;
	SpecApplied.GetAllGrantedTags(GrantedTags);

	if (GrantedTags.HasTagExact(CooldownTag))
	{
		float TimeRemaining = 0.0f;
		float Duration = 0.0f;
		GetCooldownRemainingForTag(CooldownTag, TimeRemaining, Duration);

		if (ASC->GetOwnerRole() == ROLE_Authority)
		{
			// Player is Server
			StartCooldownUpdate(CooldownTag, TimeRemaining, Duration);
			OnCooldownBegin.Broadcast(CooldownTag, TimeRemaining, Duration);
		}
		else if (!UseServerCooldown && SpecApplied.GetContext().GetAbilityInstance_NotReplicated())
		{
			// Client using predicted cooldown
			StartCooldownUpdate(CooldownTag, TimeRemaining, Duration);
			OnCooldownBegin.Broadcast(CooldownTag, TimeRemaining, Duration);
		}
		else if (UseServerCooldown && SpecApplied.GetContext().GetAbilityInstance_NotReplicated() == nullptr)
		{
			// Client using Server's cooldown. This is Server's corrective cooldown GE.
			StartCooldownUpdate(CooldownTag, TimeRemaining, Duration);
			OnCooldownBegin.Broadcast(CooldownTag, TimeRemaining, Duration);
		}
		else if (UseServerCooldown && SpecApplied.GetContext().GetAbilityInstance_NotReplicated())
		{
			// Client using Server's cooldown but this is predicted cooldown GE.
			// This can be useful to gray out abilities until Server's cooldown comes in.
			StartCooldownUpdate(CooldownTag, TimeRemaining, Duration);
			OnCooldownBegin.Broadcast(CooldownTag, -1.0f, -1.0f);
		}
	}
}

void UAsyncTaskCooldownChanged::CooldownTagChanged(const FGameplayTag InCooldownTag, int32 NewCount)
{
	if (NewCount == 0)
	{
		OnCooldownEnd.Broadcast(InCooldownTag, -1.0f, -1.0f);
		StopCooldownUpdate(CooldownTag, -1.0f, -1.0f);
		if(bEndTaskOnCooldownEnded)
		{
			UnregisterEvents();
			SetReadyToDestroy();
		}
	}
}

void UAsyncTaskCooldownChanged::StartCooldownUpdate(FGameplayTag InCooldownTag, float TimeRemaining, float Duration)
{
	if (UpdateTimerHandle.IsValid() || ActiveTimeUpdateBroadcastInterval < 0.001f) return;

	World->GetTimerManager().SetTimer(UpdateTimerHandle, this,
		&UAsyncTaskCooldownChanged::ReadAndBroadcastCooldownUpdate, ActiveTimeUpdateBroadcastInterval, true);
}

void UAsyncTaskCooldownChanged::ReadAndBroadcastCooldownUpdate()
{
	float TimeRemaining = 0.0f;
	float Duration = 0.0f;
	if(GetCooldownRemainingForTag(CooldownTag, TimeRemaining, Duration))
	{
		OnCooldownUpdate.Broadcast(CooldownTag, TimeRemaining, Duration);
	}
}

void UAsyncTaskCooldownChanged::StopCooldownUpdate(FGameplayTag InCooldownTag, float TimeRemaining, float Duration)
{
	if(UpdateTimerHandle.IsValid())
	{
		World->GetTimerManager().ClearTimer(UpdateTimerHandle);
		UpdateTimerHandle.Invalidate();
	}
}

bool UAsyncTaskCooldownChanged::GetCooldownRemainingForTag(const FGameplayTag Tag, float& TimeRemaining, float& CooldownDuration) const
{
	if (IsValid(ASC))
	{
		TimeRemaining = 0.f;
		CooldownDuration = 0.f;

		FGameplayTagContainer Container;
		Container.AddTagFast(Tag);

		FGameplayEffectQuery const Query = FGameplayEffectQuery::MakeQuery_MatchAnyOwningTags(Container);
		TArray< TPair<float, float> > DurationAndTimeRemaining = ASC->GetActiveEffectsTimeRemainingAndDuration(Query);
		if (DurationAndTimeRemaining.Num() > 0)
		{
			int32 BestIdx = 0;
			float LongestTime = DurationAndTimeRemaining[0].Key;
			for (int32 Idx = 1; Idx < DurationAndTimeRemaining.Num(); ++Idx)
			{
				if (DurationAndTimeRemaining[Idx].Key > LongestTime)
				{
					LongestTime = DurationAndTimeRemaining[Idx].Key;
					BestIdx = Idx;
				}
			}

			TimeRemaining = DurationAndTimeRemaining[BestIdx].Key;
			CooldownDuration = DurationAndTimeRemaining[BestIdx].Value;

			return true;
		}
	}

	return false;
}
