// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "Characters/TDSCharacterBase.h"
#include "TDSEnemyCharacter.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSEnemyCharacter : public ATDSCharacterBase
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDSEnemyCharacter(const class FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Enemy Character")
		void Attack();

	virtual void Attack_Implementation();

};
