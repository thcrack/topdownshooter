// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSGameStateBase.h"
#include "TDSSurvivalGameState.generated.h"

UENUM(BlueprintType)
enum class ESurvivalGamePhase : uint8
{
	Pregame,
	WaveInBound,
	WaveBreak,
	Ended
};


class UTDSEquipment;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSurvivalGameStateUpdated, class ATDSSurvivalGameState*, SurvivalGameState);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnSurvivalGamePhaseUpdated, class ATDSSurvivalGameState*, SurvivalGameState, ESurvivalGamePhase, NewPhase);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCreditsChanged, float, NewCredits);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStorageChanged, int, SlotIndex, UTDSEquipment*, NewEquipmentAtSlot);


/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ATDSSurvivalGameState : public ATDSGameStateBase
{
	GENERATED_BODY()

	const int NOT_FOUND = -1;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Survival Game State")
		ESurvivalGamePhase GamePhase;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Survival Game State")
		int CurrentWave;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Survival Game State")
		int EnemyCountLeftInWave;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Survival Game State")
		int EnemyCountSpawnedInWave;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Survival Game State")
		float WaveBreakTimeRemaining;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Survival Game State")
		float Credits;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Survival Game State")
		TArray<UTDSEquipment*> EquipmentStorage;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Survival Game State")
		int StorageSize = 12;

public:
	
	ATDSSurvivalGameState();
	
	void SetCurrentWave(int InValue);
	
	void SetEnemyCountLeftInWave(int InValue);
	
	void SetEnemyCountSpawnedInWave(int InValue);
	
	void SetWaveBreakTimeRemaining(float InValue);

	UFUNCTION(BlueprintCallable, Category = "Game Phase")
		void SetGamePhase(ESurvivalGamePhase NewPhase);

	UFUNCTION(BlueprintCallable, Category = "Credits")
		void AddCredits(float Amount);

	UFUNCTION(BlueprintCallable, Category = "Storage")
		bool HasEmptyStorageSlot() const;

	UFUNCTION(BlueprintCallable, Category = "Storage")
		UTDSEquipment* GetStorageEquipmentAt(int Index) const;

	UFUNCTION(BlueprintCallable, Category = "Storage")
		UTDSEquipment* RetrieveStorageEquipmentAt(int Index, bool bDeferBroadcast = false);
	
	UFUNCTION(BlueprintCallable, Category = "Storage")
		bool GiveStorageEquipmentToPlayer(int Index, class ATDSPlayerCharacter* PlayerCharacter);

	UFUNCTION(BlueprintCallable, Category = "Storage")
		bool SellStorageEquipment(int Index);

	UFUNCTION(BlueprintCallable, Category = "Storage")
		int FindEmptyStorageSlot() const;
	
	UFUNCTION(BlueprintCallable, Category = "Storage")
		bool PutEquipmentToStorageSlot(UTDSEquipment* Equipment, int SlotIndex);

	UFUNCTION(BlueprintCallable, Category = "Storage")
		bool PutEquipmentToEmptyStorageSlot(UTDSEquipment* Equipment);

	UPROPERTY(BlueprintAssignable, Category = "Survival Game State")
		FOnSurvivalGameStateUpdated OnSurvivalGameStateUpdated;
	
	UPROPERTY(BlueprintAssignable, Category = "Survival Game State")
		FOnSurvivalGamePhaseUpdated OnSurvivalGamePhaseUpdated;

	UPROPERTY(BlueprintAssignable, Category = "Survival Game State")
		FOnCreditsChanged OnCreditsChanged;

	UPROPERTY(BlueprintAssignable, Category = "Survival Game State")
		FOnStorageChanged OnStorageChanged;
	
};
