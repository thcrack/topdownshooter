// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "TDSSurvivalGameState.h"
#include "TDSSurvivalPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ATDSSurvivalPlayerState : public APlayerState
{
	GENERATED_BODY()

	ATDSSurvivalPlayerState();

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Survival Game State")
		float Credits;

public:

	UFUNCTION()
		float GetCredits() const { return Credits; };

	UFUNCTION(BlueprintCallable)
		void AddCredits(float Amount);

	UFUNCTION(BlueprintCallable)
		void DeductCredits(float Amount);

	UPROPERTY(BlueprintAssignable, Category = "Survival Game State")
		FOnCreditsChanged OnCreditsChanged;
	
	
};
