// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSGameModeBase.h"
#include "TDSSurvivalGameState.h"
#include "Engine/DataTable.h"
#include "TDSSurvivalGameMode.generated.h"

USTRUCT(BlueprintType)
struct FSurvivalWaveSquadInfo
{

	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		TArray<TSubclassOf<class ATDSEnemyCharacter>> Data;

};

USTRUCT(BlueprintType)
struct FSurvivalWaveSetup : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		int PodCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		bool bSpawnPodsAtNearestSpotsToPlayers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		bool bUsePredefinedSquadData;

	// Only relevant when bUsePredefinedSquadData is false
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		int TotalEnemyInWave;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		TArray <FSurvivalWaveSquadInfo> PredefinedSquads;
	
};

enum class ESurvivalGamePhase : uint8;
/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ATDSSurvivalGameMode : public ATDSGameModeBase
{
	GENERATED_BODY()

private:

	ATDSSurvivalGameState* SurvivalGameState;

	void FillCurrentWaveSquads(int EnemyCount);

protected:

	int CurrentWave = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Survival Mode")
		TSubclassOf<class ATargetPoint> PodSpawnPointClass;

	UPROPERTY()
		TArray<class ATargetPoint*> PodSpawnPoints;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Survival Mode")
		TSubclassOf<class ATDSEnemySpawnPod> EnemySpawnPodClass;
	
	UPROPERTY()
		TArray<class ATDSEnemySpawnPod*> EnemySpawnPods;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Survival Mode")
		class UDataTable* WaveSetupTable;

	UPROPERTY()
		TArray<FSurvivalWaveSetup> WaveSetups;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Survival Mode")
		TArray<TSubclassOf<class ATDSEnemyCharacter>> PossibleEnemyClasses;

	UPROPERTY()
		TArray <FSurvivalWaveSquadInfo> CurrentWaveSquads;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Survival Mode")
		int EnemyCountPerWaveBase = 5;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Survival Mode")
		int EnemyCountPerWaveCoefficient = 2;


	int EnemySquadSpawnedInWaveCount;

	int EnemyCountInCurrentWave;

	int EnemyCountLeftInWave;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Survival Mode")
		float WaveBreakDuration = 60.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Survival Mode")
		float CreditBountyPerEnemy = 15;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Survival Mode")
		float WaveStartFirstSpawnDelay = 2.f;

	void StartPlay() override;

	void BeginPlay() override;

	void StartWave();

	void EndWave();

	UFUNCTION(BlueprintCallable)
		void GameOver();

	UFUNCTION(BlueprintCallable)
	void SkipBreak();

	void SpawnEnemy();

	UFUNCTION(BlueprintCallable)
		void BindToEnemyEndPlay(AActor* Actor);

	UFUNCTION()
	void OnSpawnedEnemyEndPlay(AActor* Actor, EEndPlayReason::Type Reason);

	void SetGamePhase(ESurvivalGamePhase NewPhase);

	void OnWaveBreakTimerUpdate();

	FTimerHandle EnemySpawnTimerHandle;

	FTimerHandle WaveBreakTimerHandle;
	
	FTimerHandle WaveBreakNotifyTimerHandle;

	int TotalEnemyKilled;
	

public:
	
	ATDSSurvivalGameMode();

	UFUNCTION(BlueprintCallable)
		int GetWaveCount() const
	{
		return FMath::Max(0, CurrentWave - 1);
	}

	UFUNCTION(BlueprintCallable)
		int GetEnemyCountInCurrentWave() const
	{
		return EnemyCountInCurrentWave;
	}

	UFUNCTION(BlueprintCallable)
		int GetEnemyCountLeftInWave() const
	{
		return EnemyCountLeftInWave;
	}

	UFUNCTION(BlueprintCallable)
		float GetWaveBreakDurationLeft() const;

	UFUNCTION(BlueprintCallable)
		int GetTotalEnemyKilled() const
	{
		return TotalEnemyKilled;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
		ATDSSurvivalGameState* GetSurvivalGameState();
	
};
