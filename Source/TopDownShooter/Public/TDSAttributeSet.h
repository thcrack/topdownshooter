// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "TopDownShooter.h"
#include "AbilitySystemComponent.h"
#include "TDSAttributeSet.generated.h"

class UTPlayerAttributeSet;
/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

		DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAttributeSetReceiveDamage, float, Damage, const FGameplayEffectContextHandle&, ContextHandle);

protected:

	struct FGameplayTag energyDamageTypeTag;
	struct FGameplayTag physicalDamageTypeTag;

public:

	UTDSAttributeSet();

	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;

	bool PreGameplayEffectExecute(FGameplayEffectModCallbackData& Data) override;
	
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void InitFromMetaDataTable(const UDataTable* DataTable) override;

	//void OnAttributeAggregatorCreated(const FGameplayAttribute& Attribute, FAggregator* NewAggregator) const override;

	float LargestSlowEffectPercentage;

	UPROPERTY()
		FOnAttributeSetReceiveDamage OnAttributeSetReceiveDamage;

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, Health)

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, MaxHealth)

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		FGameplayAttributeData HealthRegenRate;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, HealthRegenRate)

	UPROPERTY(BlueprintReadOnly, Category = "Armor")
		FGameplayAttributeData Armor;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, Armor)

	UPROPERTY(BlueprintReadOnly, Category = "Armor")
		FGameplayAttributeData MaxArmor;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, MaxArmor)

	UPROPERTY(BlueprintReadOnly, Category = "Armor")
		FGameplayAttributeData ArmorRegenRate;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, ArmorRegenRate)

	UPROPERTY(BlueprintReadOnly, Category = "Armor")
		FGameplayAttributeData ArmorRegenDelay;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, ArmorRegenDelay)

	UPROPERTY(BlueprintReadOnly, Category = "Weapon")
		FGameplayAttributeData AmmoReserve;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, AmmoReserve)

	UPROPERTY(BlueprintReadOnly, Category = "Weapon")
		FGameplayAttributeData MaxAmmoReserve;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, MaxAmmoReserve)

	UPROPERTY(BlueprintReadOnly, Category = "Speed")
		FGameplayAttributeData MaxSpeed;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, MaxSpeed)

	UPROPERTY(BlueprintReadOnly, Category = "Speed")
		FGameplayAttributeData CalculatedMaxSpeed;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, CalculatedMaxSpeed)

	UPROPERTY(BlueprintReadOnly, Category = "Ability")
		FGameplayAttributeData DashCharge;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, DashCharge)

	UPROPERTY(BlueprintReadOnly, Category = "Ability")
		FGameplayAttributeData DashRechargeRate;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, DashRechargeRate)

	UPROPERTY(BlueprintReadOnly, Category = "Meta")
		FGameplayAttributeData Damage;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, Damage)

	UPROPERTY(BlueprintReadOnly, Category = "Meta")
		FGameplayAttributeData SlowPercentage;
	ATTRIBUTE_ACCESSORS(UTDSAttributeSet, SlowPercentage)
	
};
