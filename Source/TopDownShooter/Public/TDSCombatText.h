// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSCombatText.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSCombatText : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDSCombatText();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		class UWidgetComponent* WidgetComponent;

	bool bLaunched;

	UPROPERTY()
		FText Text;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void SetText(const FText& InText);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void OnSetText(const FText& InText);

	UFUNCTION(BlueprintCallable)
		void Launch();

	UFUNCTION(BlueprintImplementableEvent)
		void OnLaunch();

};
