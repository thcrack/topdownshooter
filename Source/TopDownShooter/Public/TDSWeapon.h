// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSWeaponAttributeSet.h"
#include "AbilitySystemInterface.h"
#include "TDSWeapon.generated.h"

UENUM(BlueprintType)
enum class EWeaponStance : uint8
{
	WS_Empty 		UMETA(DisplayName = "Empty Stance"),
	WS_Rifle 		UMETA(DisplayName = "Rifle Stance"),
	WS_Pistol 		UMETA(DisplayName = "Pistol Stance"),
};

UCLASS(BlueprintType)
class TOPDOWNSHOOTER_API ATDSWeapon : public AActor
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Weapon)
		EWeaponStance WeaponStance;
	
	// Sets default values for this actor's properties
	ATDSWeapon();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, BlueprintPure, Category = Weapon)
		FTransform GetWeaponFirePoint();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Weapon)
		bool AttachToWeaponFirePoint(class USceneComponent* Component);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Weapon)
		TArray<class USoundBase*> SoundArray;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Weapon)
		class USoundBase* GetRandomSound();

};
