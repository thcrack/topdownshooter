// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "TDSGEECConvertMagazineToReserve.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSGEECConvertMagazineToReserve : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

public:
	UTDSGEECConvertMagazineToReserve();

	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
};
