// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "TDSGEECConvertReserveToMagazine.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSGEECConvertReserveToMagazine : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
	
public:
	
	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
};
