// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "TDSMMCHealthBasedSlow.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSMMCHealthBasedSlow : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

	UTDSMMCHealthBasedSlow();

	bool HasCalcedOnce;
	float OldVal;

	FGameplayEffectAttributeCaptureDefinition HealthDef;
	FGameplayEffectAttributeCaptureDefinition MaxHealthDef;

	float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;
	
};
