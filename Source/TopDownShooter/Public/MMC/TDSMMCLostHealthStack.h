// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "TDSMMCLostHealthStack.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSMMCLostHealthStack : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

	UTDSMMCLostHealthStack();

	FGameplayEffectAttributeCaptureDefinition ArmorDef;
	FGameplayEffectAttributeCaptureDefinition HealthDef;
	FGameplayEffectAttributeCaptureDefinition MaxHealthDef;

	float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;
};
