// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "AbilitySystemInterface.h"
#include "TDSPawnBase.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSPawnBase : public APawn, public IAbilitySystemInterface
{
	
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UTDSAbilitySystemComponent* AbilitySystem;

public:
	// Sets default values for this pawn's properties
	ATDSPawnBase();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UAbilitySystemComponent* GetAbilitySystemComponent() const override;

};
