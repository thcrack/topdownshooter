// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TDSCharacterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:

	float GetMaxSpeed() const override;
	
};
