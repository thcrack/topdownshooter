// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDSGameModeBase.generated.h"

enum class ESlateVisibility : uint8;
/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API ATDSGameModeBase : public AGameModeBase
{
public:
	ATDSGameModeBase();

private:
	GENERATED_BODY()

protected:

	/** Actors that should be included in the viewport when moving the camera**/
	TArray<AActor*> ImportantActors;

	class ATDSCameraAnchor* CameraAnchor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<class ATDSCameraAnchor> CameraAnchorClass;

public:

	void BeginPlay() override;

	virtual void WatchDefaultActors();

	UFUNCTION(BlueprintCallable)
		void AddActorToWatch(AActor* Actor);

	UFUNCTION(BlueprintCallable)
		void RemoveActorToWatch(AActor* Actor);

	UFUNCTION(BlueprintCallable)
	void SetWidgetLayerVisibility(FName LayerName, ESlateVisibility Visibility);

	void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
	void SetViewTargetWithBlend(AActor* NewViewTarget, float BlendTime, EViewTargetBlendFunction BlendFunc,
	                            float BlendExp,
	                            bool bLockOutgoing);
};
