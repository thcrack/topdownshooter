// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "Interfaces/TDSFactionInterface.h"
#include "Interfaces/TDSDamageCauserCallbackInterface.h"
#include "GameplayEffectTypes.h"
#include "TDSCharacterBase.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSCharacterBase : public ACharacter,
	public IAbilitySystemInterface,
	public ITDSFactionInterface,
	public ITDSDamageCauserCallbackInterface
{
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnCausedDamageCallback, float, Damage, const FGameplayEffectContextHandle&, ContextHandle);
	
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDSCharacterBase(const class FObjectInitializer& ObjectInitializer);

protected:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Faction)
	ETDSFaction Faction;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Ability System")
		class UTDSAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
		class UDataTable* AttributeInitDataTable;

	/** Combat Text **/

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat Text")
		bool bCreateCombatText;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat Text")
		TSubclassOf<class ATDSCombatText> CombatTextClass;

	UFUNCTION(Category = "Combat Text")
		virtual void OnReceiveDamage(float Damage, const FGameplayEffectContextHandle& ContextHandle);

	UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName = "On Receive Damage"))
		void K2_OnReceiveDamage(float Damage, const FGameplayEffectContextHandle& ContextHandle);
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Faction")
		ETDSFaction GetFaction();

	UPROPERTY(BlueprintAssignable, Category = "Damage Causer")
		FOnCausedDamageCallback OnCausedDamageCallback;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Damage Causer")
		void CausedDamageCallback(float Damage, const FGameplayEffectContextHandle& ContextHandle);

};
