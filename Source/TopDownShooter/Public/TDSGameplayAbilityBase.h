// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "TDSGameplayAbilityBase.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSGameplayAbilityBase : public UGameplayAbility
{
	GENERATED_BODY()

protected:

	FGameplayTag CooldownSetByCallerTag;

	UPROPERTY(EditDefaultsOnly, Category = "Ability")
		class UCompositeCurveTable* StatTable;
	
public:
	UTDSGameplayAbilityBase();

	UFUNCTION(BlueprintCallable, Category = "Ability")
		class UCompositeCurveTable* GetStatTable() const;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Ability")
		bool bIgnoreCostForActivation;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Ability")
		FScalableFloat CooldownDuration;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Ability")
		FGameplayTagContainer CooldownTags;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Ability")
		bool bInheritCooldownTags;

	virtual FGameplayTagContainer* GetCooldownTags() const override;

	virtual void ApplyCooldown(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) const override;

	bool CheckCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, FGameplayTagContainer* OptionalRelevantTags) const override;
	
	// Tells an ability to activate immediately when its granted. Used for passive abilities and abilites forced on others.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Ability")
		bool ActivateAbilityOnGranted = false;

	// If an ability is marked as 'ActivateAbilityOnGranted', activate them immediately when given here
	// Epic's comment: Projects may want to initiate passives or do other "BeginPlay" type of logic here.
	void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;

};
