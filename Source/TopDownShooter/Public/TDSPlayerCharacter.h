// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "GameplayEffectTypes.h"
#include "GameplayAbilitySpec.h"
#include "Characters/TDSCharacterBase.h"
#include "TDSWeapon.h"
#include "Equipment/TDSEquipment.h"
#include "Equipment/TDSArmorEquipment.h"
#include "Equipment/TDSAbilityEquipment.h"
#include "Equipment/TDSWeaponEquipment.h"
#include "Interfaces/TDSWeaponHolderInterface.h"
#include "TDSPlayerCharacter.generated.h"

UENUM(BlueprintType)
enum class EEquipAbilityStatus : uint8
{
	EAS_Equip = 0		UMETA(DisplayName = "Can Equip"),
	EAS_Upgrade = 1		UMETA(DisplayName = "Can Upgrade"),
	EAS_Override = 2	UMETA(DisplayName = "Can Override"),
	EAS_Reject = 3		UMETA(DisplayName = "Cannot Equip")
};

USTRUCT(BlueprintType)
struct FPlayerCharacterInfo : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		bool bInUse;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		FText CharacterName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		class USkeletalMesh* Mesh;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnGameplayAttributeValueChangeBroken, const float, NewValue, const float,
OldValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEquippedWeaponChange, const UTDSWeaponEquipment*, NewWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEquippedArmorChange, const UTDSArmorEquipment*, NewArmor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEquippedAbilityChange, const UTDSAbilityEquipment*, NewAbility);


struct FOnAttributeChangeData;
UCLASS()
class TOPDOWNSHOOTER_API ATDSPlayerCharacter : public ATDSCharacterBase, public ITDSWeaponHolderInterface
{
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerCharacterIncapacitated, const ATDSPlayerCharacter*, PlayerCharacter);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerCharacterRevived, const ATDSPlayerCharacter*, PlayerCharacter);
	
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDSPlayerCharacter(const class FObjectInitializer& ObjectInitializer);

protected:

	FPlayerCharacterInfo PlayerCharacterInfo;

	/** Components **/

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		class UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Widgets)
		class UWidgetComponent* FloatingWidget;

	/** Weapon **/

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon")
		class UTDSWeaponEquipment* EquippedWeapon;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon")
		class ATDSWeapon* EquippedWeaponActor;

	FGameplayAbilitySpecHandle WeaponMainAbilityHandle;
	
	FGameplayAbilitySpecHandle WeaponOffhandAbilityHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		TSubclassOf<class UGameplayAbility> ReloadAbility;

	/** Armor **/
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor")
		TSubclassOf<class UGameplayAbility> ArmorRegenAbility;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Armor")
		class UTDSArmorEquipment* EquippedArmor;

	/** Abilities **/
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
		TSubclassOf<class UGameplayEffect> DashRechargeEffect;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		UTDSAbilityEquipment* EquippedMovementAbility;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
		TSubclassOf<class UGameplayAbility> MovementAbility;

	FGameplayAbilitySpecHandle MovementAbilityHandle;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		UTDSAbilityEquipment* EquippedActiveAbility;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
		TSubclassOf<class UGameplayAbility> ActiveAbility;

	FGameplayAbilitySpecHandle ActiveAbilityHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
		TSubclassOf<class UGameplayAbility> ReactiveAbility;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		UTDSAbilityEquipment* EquippedReactiveAbility;

	FGameplayAbilitySpecHandle ReactiveAbilityHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
		TSubclassOf<class UGameplayAbility> PassiveAbility;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		UTDSAbilityEquipment* EquippedPassiveAbility;

	FGameplayAbilitySpecHandle PassiveAbilityHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
		TSubclassOf<class UGameplayAbility> OverdriveAbility;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		UTDSAbilityEquipment* EquippedOverdriveAbility;

	FGameplayAbilitySpecHandle OverdriveAbilityHandle;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float Value);

	void MoveRight(float Value);

	void ApplyDefaultAbilities();

	bool bIncapacitated;

	void OnHealthChanged(const FOnAttributeChangeData& Data);

	UFUNCTION()
		void OnIncapacitated();
	
	UFUNCTION(BlueprintImplementableEvent, Category=Events)
		void OnIncapacitatedImplementable();

	UFUNCTION()
		void OnRevived();

	UFUNCTION(BlueprintImplementableEvent, Category = Events)
		void OnRevivedImplementable();

	UFUNCTION(BlueprintImplementableEvent, Category = Events)
		void OnWeaponActorChanged(class ATDSWeapon* NewActor);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, Category = "Character Info")
		FPlayerCharacterInfo GetCharacterInfo() const;

	UFUNCTION(BlueprintCallable, Category = "Character Info")
		void SetCharacterInfo(const FPlayerCharacterInfo& InInfo);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Weapon)
	const FWeaponData GetEquippedWeaponData();

	UPROPERTY(BlueprintAssignable, Category = Weapon)
	FOnEquippedWeaponChange OnEquippedWeaponChange;

	UFUNCTION(BlueprintCallable, Category = Weapon)
		EWeaponStance GetWeaponStance() const;

	ATDSWeapon* GetWeaponActor() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Armor)
	const FArmorData GetEquippedArmorData();

	UPROPERTY(BlueprintAssignable, Category = Armor)
	FOnEquippedArmorChange OnEquippedArmorChange;

	UPROPERTY(BlueprintAssignable, Category = Ability)
	FOnEquippedAbilityChange OnEquippedAbilityChange;

	UFUNCTION(BlueprintCallable, Category = "Ability Equipment")
		EEquipAbilityStatus CheckEquipAbilityStatus(const UTDSAbilityEquipment* InEquipment);

	UFUNCTION(BlueprintCallable, Category = "Ability Equipment")
		int GetEquippedAbilityLevel(EAbilityType AbilityType) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Weapon)
	bool HasEquippedWeapon() const
	{
		return EquippedWeapon != nullptr;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Armor)
	bool HasEquippedArmor() const
	{
		return EquippedArmor != nullptr;
	}

	UFUNCTION(BlueprintCallable, Category = Weapon)
		bool EquipWeapon(UTDSWeaponEquipment* WeaponEquipment, bool bForceEquip = false);

	UFUNCTION(BlueprintCallable, Category = Armor)
		bool EquipArmor(UTDSArmorEquipment* ArmorEquipment, bool bForceEquip = false);

	UFUNCTION(BlueprintCallable, Category = Ability)
		bool EquipAbility(UTDSAbilityEquipment* AbilityEquipment);

	UFUNCTION(BlueprintCallable, Category = Weapon)
		bool UnequipWeapon(UTDSWeaponEquipment*& OutEquipment, bool bDeferBroadcast = false);

	UFUNCTION(BlueprintCallable, Category = Armor)
		bool UnequipArmor(UTDSArmorEquipment*& OutEquipment, bool bDeferBroadcast = false);

	UFUNCTION(BlueprintCallable, Category = "Equipments")
		bool UnequipAndHandleEquipment(UTDSEquipment* Equipment, EExistingEquipmentPolicy ExistingEquipmentPolicy);

	int PlayerId;

	UFUNCTION(BlueprintCallable, Category = "Player Character")
		bool IsIncapacitated() const;

	UFUNCTION(BlueprintCallable, Category = "Player Character")
		void Revive();

	UPROPERTY(BlueprintAssignable, Category = Events)
		FOnPlayerCharacterIncapacitated OnPlayerCharacterIncapacitated;

	UPROPERTY(BlueprintAssignable, Category = Events)
		FOnPlayerCharacterRevived OnPlayerCharacterRevived;

	virtual void PossessedBy(AController* NewController) override;

};
