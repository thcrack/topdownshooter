// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "TDSWeapon.h"
#include "Equipment/TDSArmorEquipment.h"
#include "TDSAbilitySystemComponent.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()

		UTDSAbilitySystemComponent(const FObjectInitializer& ObjectInitializer);

protected:

	bool bHasWeaponAttributes;

	void CheckOrInitWeaponAttributes();
	
public:

	void RefreshWithWeaponData(const FWeaponData& InData);
	void RefreshWithWeaponDynamicData(const FWeaponDynamicData& InData);
	void RefreshWithArmorData(const FArmorData& InData);
	void UpdateWeaponDynamicData(FWeaponDynamicData& InData);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Abilities)
		const bool IsAbilityActiveByClass(TSubclassOf<class UGameplayAbility> AbilityClass);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Abilities)
		const bool IsAbilityActiveByHandle(FGameplayAbilitySpecHandle& AbilitySpecHandle);

	virtual void LocalInputConfirm() override;
	
};
