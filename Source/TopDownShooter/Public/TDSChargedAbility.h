// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSGameplayAbilityBase.h"
#include "TDSChargedAbility.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSChargedAbility : public UTDSGameplayAbilityBase
{
	GENERATED_BODY()

protected:

	UTDSChargedAbility();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<class UGameplayEffect> ChargeEffect;

	FActiveGameplayEffectHandle ChargeEffectHandle;
	
	FActiveGameplayEffectHandle RechargeCooldownHandle;

	void StartRecharge(const FGameplayAbilityActorInfo* ActorInfo);

	void OnRecharge(const FGameplayEffectRemovalInfo& RemovedInfo);

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		int MaxChargeCount = 3;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		int ChargeCountOnStart = 3;

	int GetStackCount(const FGameplayAbilityActorInfo* ActorInfo) const;

	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	
	void ApplyCooldown(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) const override;

	void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;

	void CommitExecute(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) override;
	
};
