// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "TDSGameplayAbilityTargetActor.generated.h"

/**
 * 
 */
UCLASS(Abstract, Blueprintable)
class TOPDOWNSHOOTER_API ATDSGameplayAbilityTargetActor : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()

public:

	virtual void ConfirmTargeting() override;

	virtual void ConfirmTargetingAndContinue() override;

	UFUNCTION(BlueprintImplementableEvent)
		FGameplayAbilityTargetDataHandle MakeTargetDataHandle();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure)
		bool CanBeConfirmed();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual bool IsConfirmTargetingAllowed() override;

protected:
	
};
