// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
struct FSurvivalWaveSquadInfo;

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSEnemyCharacter.h"
#include "TDSEnemySpawnPod.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSEnemySpawnPod : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDSEnemySpawnPod();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
		USceneComponent* GetSpawnPointSceneComponent(int Index);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		TArray<class ATDSEnemyCharacter*> SpawnSquad(const TArray<TSubclassOf<ATDSEnemyCharacter>>& SquadInfo);

	UFUNCTION(BlueprintImplementableEvent)
		void Retreat();

};
