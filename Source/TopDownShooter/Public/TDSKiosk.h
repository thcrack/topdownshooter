// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSEquipment.h"
#include "TDSKiosk.generated.h"

enum class EExistingEquipmentPolicy : uint8;

UCLASS()
class TOPDOWNSHOOTER_API ATDSKiosk : public AActor
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnSellingItemsChanged, int32, Index, UTDSEquipment*, NewEquipment);
	
public:	
	// Sets default values for this actor's properties
	ATDSKiosk();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Kiosk|Settings")
		bool bKioskEnabled;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Kiosk|Settings")
		int PlayersRequiredToOpen = 2;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Kiosk|Settings")
		float TimeBeforeOpeningUI;

	UPROPERTY()
		FString ContextString;

	FTimerHandle OpenUITimerHandle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UStaticMeshComponent* MeshComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class USphereComponent* PlayerDetectionSphere;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Selling Items")
		int SellingItemsMaxCount = 16;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Selling Items")
		TArray<UTDSEquipment*> SellingItems;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Selling Items")
		UTDSEquipment* OnSaleItem;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Selling Items|Source")
		class UDataTable* ArmorTable;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Selling Items|Source")
		class UDataTable* WeaponTable;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Selling Items|Source")
		class UDataTable* AbilityTable;
	
	// Called when the game starts or when spawned
	void BeginPlay() override;

	UFUNCTION()
		int GetCurrentPlayersInRangeCount(bool bUseComponent);

	UFUNCTION(BlueprintCallable)
		void EnableKiosk();

	UFUNCTION(BlueprintCallable)
		void DisableKiosk();

	UFUNCTION()
		void AttemptOpenKiosk();

	UFUNCTION(BlueprintImplementableEvent)
		void OnKioskEnabled();

	UFUNCTION(BlueprintImplementableEvent)
		void OnKioskDisabled();

	UFUNCTION()
		void OnActorEntersDetectionSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnActorLeavesDetectionSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintImplementableEvent)
		void OnOpenUI();
	
	UFUNCTION()
		void OnOpenUI_Internal();

	UFUNCTION(BlueprintCallable)
		void Restock();
	
	void FillSellingItems();

	UPROPERTY(BlueprintAssignable, Category = "Selling Items")
	FOnSellingItemsChanged OnSellingItemsChanged;

public:

	UFUNCTION(BlueprintCallable, Category = "Selling Items")
		void RefreshAllSellingItems();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Selling Items")
		TArray<UTDSEquipment*> GetSellingItems() const { return SellingItems; };

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Selling Items")
		bool CanPurchaseSellingItem(int Index, int PlayerId);

	UFUNCTION(BlueprintCallable, Category = "Selling Items")
		UTDSEquipment* PurchaseSellingItem(int Index, int PlayerId);

	UFUNCTION(BlueprintCallable, Category = "Selling Items")
		UTDSEquipment* PurchaseSellingItemChecked(int Index, int PlayerId);

	UFUNCTION(BlueprintCallable, Category = "Selling Items")
		UTDSEquipment* PurchaseSellingItemWithEquipment(UTDSEquipment* Equipment, int PlayerId);

	UFUNCTION(BlueprintCallable, Category = "Selling Items")
		bool PurchaseSellingItemAndGiveToPlayerCharacter(int Index, class ATDSPlayerCharacter* PlayerCharacter, EExistingEquipmentPolicy ExistingEquipmentPolicy);

	UFUNCTION(BlueprintCallable, Category = "Selling Items")
		bool PurchaseSellingItemAndGiveToStorage(int Index, class ATDSPlayerCharacter* PlayerCharacter);

};
