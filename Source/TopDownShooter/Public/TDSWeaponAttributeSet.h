// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "TopDownShooter.h"
#include "AbilitySystemComponent.h"
#include "TDSEquipment.h"
#include "TDSWeaponAttributeSet.generated.h"

UENUM(BlueprintType)
enum class EWeaponReloadType : uint8
{
	WRT_FullMag 	UMETA(DisplayName = "Full Magazine"),
	WRT_PerAmmo 	UMETA(DisplayName = "Per Ammo"),
};

UENUM(BlueprintType)
enum class EWeaponDamageType : uint8
{
	WDT_Physical	UMETA(DisplayName = "Physical Damage"),
	WDT_Energy		UMETA(DisplayName = "Energy Damage"),
};

UENUM(BlueprintType)
enum class EWeaponEquipmentType : uint8
{
	WET_TwoHand 	UMETA(DisplayName = "Two-hand"),
	WET_OneHand 	UMETA(DisplayName = "One-hand"),
	WET_Offhand 	UMETA(DisplayName = "Offhand"),
};

UENUM(BlueprintType)
enum class EWeaponCategory : uint8
{
	WC_None = 0			UMETA(DisplayName = "None"),
	WC_Pistol = 1		UMETA(DisplayName = "Pistol"),
	WC_Rifle = 2		UMETA(DisplayName = "Rifle"),
	WC_Shotgun = 3		UMETA(DisplayName = "Shotgun"),
	WC_Sniper_Rifle = 4	UMETA(DisplayName = "Sniper Rifle"),
	WC_Other = 5		UMETA(DisplayName = "Other"),
};

USTRUCT(BlueprintType)
struct FWeaponData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Meta)
		FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		UTexture2D* Icon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attribute)
		EEquipmentRarity Rarity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attribute)
		float Price;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		TSubclassOf<class ATDSWeapon> WeaponActorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		TSubclassOf<class UGameplayAbility> AbilityClass;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		EWeaponEquipmentType EquipmentType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		EWeaponCategory WeaponCategory;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Output)
		EWeaponDamageType WeaponDamageType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Output)
		float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Output)
		int PelletsPerShot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Output)
		int FirePerTrigger; // 0 = Full auto, 1 = Single, 2+ = Burst

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Output)
		float RateOfFire;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Output)
		float Accuracy;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
		float MagazineSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
		float ReloadTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
		EWeaponReloadType ReloadType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
		float AmmoReserveCost;
};

USTRUCT(BlueprintType)
struct FWeaponDynamicData
{
	GENERATED_USTRUCT_BODY()

public:

	FWeaponDynamicData() = default;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float AmmoInMagazine;
};

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSWeaponAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:

	UTDSWeaponAttributeSet();

	UPROPERTY(BlueprintReadOnly, Category = "Ammo")
	FGameplayAttributeData AmmoInMagazine;
	ATTRIBUTE_ACCESSORS(UTDSWeaponAttributeSet, AmmoInMagazine)
	
	UPROPERTY(BlueprintReadOnly, Category = "Ammo")
	FGameplayAttributeData MagazineSize;
	ATTRIBUTE_ACCESSORS(UTDSWeaponAttributeSet, MagazineSize)

	UPROPERTY(BlueprintReadOnly, Category = "Ammo")
	FGameplayAttributeData ReloadTime;
	ATTRIBUTE_ACCESSORS(UTDSWeaponAttributeSet, ReloadTime)

	UPROPERTY(BlueprintReadOnly, Category = "Ammo")
	FGameplayAttributeData AmmoReserveCost;
	ATTRIBUTE_ACCESSORS(UTDSWeaponAttributeSet, AmmoReserveCost)

	UPROPERTY(BlueprintReadOnly, Category = "Attack")
	FGameplayAttributeData Damage;
	ATTRIBUTE_ACCESSORS(UTDSWeaponAttributeSet, Damage)

	UPROPERTY(BlueprintReadOnly, Category = "Attack")
	FGameplayAttributeData PelletsPerShot;
	ATTRIBUTE_ACCESSORS(UTDSWeaponAttributeSet, PelletsPerShot)

	UPROPERTY(BlueprintReadOnly, Category = "Attack")
	FGameplayAttributeData RateOfFire;
	ATTRIBUTE_ACCESSORS(UTDSWeaponAttributeSet, RateOfFire)

	UPROPERTY(BlueprintReadOnly, Category = "Attack")
	FGameplayAttributeData Accuracy;
	ATTRIBUTE_ACCESSORS(UTDSWeaponAttributeSet, Accuracy)

	UPROPERTY(BlueprintReadOnly, Category = "Self-Slow")
	FGameplayAttributeData SelfSlowAmount;
	ATTRIBUTE_ACCESSORS(UTDSWeaponAttributeSet, SelfSlowAmount)

	UPROPERTY(BlueprintReadOnly, Category = "Self-Slow")
	FGameplayAttributeData SelfSlowLingerDuration;
	ATTRIBUTE_ACCESSORS(UTDSWeaponAttributeSet, SelfSlowLingerDuration)
	
};
