// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameplayEffectTypes.h"
#include "TDSBlueprintFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category="Effect Context")
		static void AddInstigator(FGameplayEffectContextHandle Context, AActor* Instigator,
			AActor* EffectCauser, FGameplayEffectContextHandle& OutContext);

	UFUNCTION(BlueprintCallable, Category = "Effect Context")
		static FGameplayEffectContextHandle MakeEffectContext(AActor* Instigator, AActor* EffectCauser);

	UFUNCTION(BlueprintPure, Category = "Scalable Float")
		static float EvaluateScalableFloat(const FScalableFloat& ScalableFloat, float Level);

	UFUNCTION(BlueprintPure, Category = "Scalable Float")
		static bool IsEffectHandleValid(const FActiveGameplayEffectHandle& ActiveGameplayEffectHandle);
};
