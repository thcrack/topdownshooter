// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDSFactionInterface.generated.h"

UENUM(BlueprintType)
enum class ETDSFaction : uint8
{
	None,
	GoodGuys,
	BadGuys,
	Team1,
	Team2
};

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class UTDSFactionInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TOPDOWNSHOOTER_API ITDSFactionInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Faction)
		ETDSFaction GetFaction();
};
