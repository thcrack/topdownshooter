// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDSKioskWidget.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSKioskWidget : public UUserWidget
{
	GENERATED_BODY()

	UTDSKioskWidget(const FObjectInitializer& ObjectInitializer);
	
};
