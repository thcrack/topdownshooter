// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment/TDSEquipment.h"
#include "TDSGameplayAbilityBase.h"
#include "TopDownShooter.h"
#include "TDSAbilityEquipment.generated.h"

USTRUCT(BlueprintType)
struct FAbilityData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		UTexture2D* Icon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		TSubclassOf<UTDSGameplayAbilityBase> AbilityClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		EAbilityType AbilityType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		float Price;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability Equipment")
		FText MaxLevelDescription;

};

/**
 * 
 */
UCLASS(BlueprintType)
class TOPDOWNSHOOTER_API UTDSAbilityEquipment : public UTDSEquipment
{
	GENERATED_BODY()

protected:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability Data")
		FAbilityData AbilityData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability Data")
		int AbilityLevel;

	FText MakeFormattedDescription() const override;
	
public:

	UTDSAbilityEquipment();

	FText GetName() override;
	
	class UTexture2D* GetIcon() override;

	float GetPrice() override;

	EEquipmentRarity GetRarity() override;
	
	EEquipmentType GetType() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Ability Equipment")
		bool CanUpgrade() const;

	UFUNCTION(BlueprintCallable, Category = "Ability Equipment")
		bool Upgrade();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Ability Equipment")
		bool IsSame(const UTDSAbilityEquipment* Other);

	UFUNCTION(BlueprintCallable, Category = "Ability Equipment")
		static UTDSAbilityEquipment* MakeAbilityEquipment(FAbilityData InAbilityData, int InitLevel = 0);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Ability Equipment")
		FAbilityData GetAbilityData() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Ability Equipment")
		TSubclassOf<UTDSGameplayAbilityBase> GetAbility() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Ability Equipment")
		const int GetAbilityLevel() const;
};
