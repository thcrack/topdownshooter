// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment/TDSEquipment.h" 	
#include "Engine/World.h"
#include "TDSWeapon.h"
#include "TDSWeaponEquipment.generated.h"

/**
 * 
 */

UCLASS(BlueprintType)
class TOPDOWNSHOOTER_API UTDSWeaponEquipment : public UTDSEquipment
{
private:
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Data")
		FWeaponData WeaponData;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Weapon Data")
		FWeaponDynamicData WeaponDynamicData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Data")
		class TSubclassOf<class UGameplayAbility> WeaponAbility;

	FText MakeFormattedDescription() const override;

public:

	UTDSWeaponEquipment();

	FText GetName() override;

	class UTexture2D* GetIcon() override;

	float GetPrice() override;

	EEquipmentRarity GetRarity() override;

	EEquipmentType GetType() override;
	
	UFUNCTION(BlueprintCallable, Category = "Weapon Equipment")
	static UTDSWeaponEquipment* MakeWeaponEquipment(FWeaponData InWeaponData);

	ATDSWeapon* MakeWeaponActor(UWorld* WorldContext, const FTransform& Transform,
		const FActorSpawnParameters& SpawnParams);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Weapon Data")
		TSubclassOf<UGameplayAbility> GetWeaponAbility() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Weapon Data")
		const FWeaponData& GetWeaponData() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Weapon Data")
		const FWeaponDynamicData& GetWeaponDynamicData() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Weapon Data")
		FWeaponDynamicData& GetWeaponDynamicDataRef();
	
};
