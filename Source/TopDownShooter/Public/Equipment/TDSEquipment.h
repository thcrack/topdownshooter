// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TDSEquipment.generated.h"

UENUM(BlueprintType)
enum class EEquipmentRarity : uint8
{
	EER_Common = 0		UMETA(DisplayName = "Common"),
	EER_Rare = 1		UMETA(DisplayName = "Rare"),
	EER_Exquisite = 2	UMETA(DisplayName = "Exquisite"),
	EER_Legendary = 3	UMETA(DisplayName = "Legendary")
};

UENUM(BlueprintType)
enum class EEquipmentType : uint8
{
	EET_None = 0		UMETA(DisplayName = "None"),
	EET_Weapon = 1		UMETA(DisplayName = "Weapon"),
	EET_WeaponMod = 2	UMETA(DisplayName = "Weapon Mod"),
	EET_Armor = 3		UMETA(DisplayName = "Armor"),
	EET_Ability = 4	UMETA(DisplayName = "Ability")
};

/**
 * 
 */
UCLASS(BlueprintType)
class TOPDOWNSHOOTER_API UTDSEquipment : public UObject
{
	GENERATED_BODY()

protected:
	
	bool bHasCachedFormattedDescription = false;

	FText CachedFormattedDescription;

	UFUNCTION()
		virtual FText MakeFormattedDescription() const;
	
public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Equipment)
		virtual FText GetName();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Equipment)
		virtual class UTexture2D* GetIcon();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category= Equipment)
		virtual float GetPrice();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Equipment)
		virtual EEquipmentRarity GetRarity();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Equipment)
		virtual EEquipmentType GetType();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Equipment)
		FText GetFormattedDescription();

	UFUNCTION(BlueprintCallable, Category = Equipment)
		bool InvalidateDescription();
};
