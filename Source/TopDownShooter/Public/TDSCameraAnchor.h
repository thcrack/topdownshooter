// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSCameraAnchor.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSCameraAnchor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDSCameraAnchor();

private:

	float TargetHeight;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		class UCameraComponent* ViewTarget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		class USpringArmComponent* CameraArm;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera Anchor")
		float CameraMinHeight;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera Anchor")
		float CameraMaxHeight;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera Anchor")
		float ActorInclusionMargin;

public:	

	UFUNCTION(BlueprintCallable, Category = "Camera Anchor")
		void UpdateAnchor(const TArray<AActor*>& ActorsToFit, float DeltaSeconds);

};
