// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"

//UMG
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
//

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

UENUM(BlueprintType)
enum class EPlayerAbilityInput : uint8
{
	None UMETA(DisplayName = "None"),
	UseMovementAbility UMETA(DisplayName = "Use Movement Ability"),
	UseActiveAbility UMETA(DisplayName = "Use Ability"),
	UseWeaponMainAbility UMETA(DisplayName = "Use Weapon Main Ability"),
	UseWeaponOffhandAbility UMETA(DisplayName = "Use Weapon Offhand Ability"),
	UseOverdriveAbility UMETA(DisplayName = "Use Overdrive Ability"),
	Reload UMETA(DisplayName = "Reload")
};

UENUM(BlueprintType)
enum class EAbilityType : uint8
{
	AT_Movement UMETA(DisplayName = "Movement Ability"),
	AT_Active UMETA(DisplayName = "Active Ability"),
	AT_Reactive UMETA(DisplayName = "Reactive Ability"),
	AT_Passive UMETA(DisplayName = "Passive Ability"),
	AT_Overdrive UMETA(DisplayName = "Overdrive Ability"),
};

UENUM(BlueprintType)
enum class EExistingEquipmentPolicy : uint8
{
	EEEP_Sell = 0		UMETA(DisplayName = "Sell"),
	EEEP_ToStorage = 1		UMETA(DisplayName = "Send To Storage")
};
