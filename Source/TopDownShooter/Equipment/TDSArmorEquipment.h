// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment/TDSEquipment.h"
#include "Engine/DataTable.h"
#include "Engine/Texture2D.h"
#include "TDSArmorEquipment.generated.h"

USTRUCT(BlueprintType)
struct FArmorData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Meta)
		UTexture2D* Icon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attribute)
		EEquipmentRarity Rarity;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attribute)
		float Price;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attribute)
		FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attribute)
		float MaxArmor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attribute)
		float RegenRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attribute)
		float RegenDelay;
	
};

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSArmorEquipment : public UTDSEquipment
{
	GENERATED_BODY()

protected:

	FArmorData ArmorData;

	FText MakeFormattedDescription() const override;

public:

	FText GetName() override;

	class UTexture2D* GetIcon() override;

	float GetPrice() override;

	EEquipmentRarity GetRarity() override;

	EEquipmentType GetType() override;

	UFUNCTION(BlueprintCallable, Category = "Armor Equipment")
		static UTDSArmorEquipment* MakeArmorEquipment(FArmorData InArmorData);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Armor Data")
		const FArmorData& GetArmorData() const;
	
};
