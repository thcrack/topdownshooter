// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSArmorEquipment.h"

FText UTDSArmorEquipment::GetName()
{
	return ArmorData.Name;
}

UTexture2D* UTDSArmorEquipment::GetIcon()
{
	return ArmorData.Icon;
}

float UTDSArmorEquipment::GetPrice()
{
	return ArmorData.Price;
}

EEquipmentRarity UTDSArmorEquipment::GetRarity()
{
	return ArmorData.Rarity;
}

EEquipmentType UTDSArmorEquipment::GetType()
{
	return EEquipmentType::EET_Armor;
}

FText UTDSArmorEquipment::MakeFormattedDescription() const
{
	auto Format = GetArmorData().Description;

	FFormatNamedArguments Arguments;
	Arguments.Add(TEXT("max_armor"), GetArmorData().MaxArmor);
	Arguments.Add(TEXT("regen_rate"), GetArmorData().RegenRate);
	Arguments.Add(TEXT("regen_delay"), GetArmorData().RegenDelay);

	return FText::Format(Format, Arguments);
}


UTDSArmorEquipment* UTDSArmorEquipment::MakeArmorEquipment(FArmorData InArmorData)
{
	UTDSArmorEquipment* NewWeaponEquipment = NewObject<UTDSArmorEquipment>();
	NewWeaponEquipment->ArmorData = InArmorData;
	return NewWeaponEquipment;
}

const FArmorData& UTDSArmorEquipment::GetArmorData() const
{
	return ArmorData;
}
